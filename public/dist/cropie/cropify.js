function demoUpload() {
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.hideOnAction').hide();
                $('#upload-demo').addClass('ready');
                $('#selected_image_detail').show();
                $('#selected_image_name').html(input.files[0].name);
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 150,
            height: 150,
            type: 'square'
        },
        enforceBoundary:true,
        enableResize:true,
        enableZoom:true,
        showZoomer:true,
        enableExif: true
    });
    var croppieDetails = $uploadCrop.croppie('get');
    var zoom = croppieDetails['zoom']; console.log(zoom);
    $uploadCrop.croppie('setZoom', zoom);
    $('#zoomPlus').on('click', function () {
        zoom = zoom + 0.01;console.log(zoom);
        $uploadCrop.croppie('setZoom', zoom);
    });

    $('#zoomMinus').on('click', function () {
       zoom = zoom - 0.01;console.log(zoom);
        $uploadCrop.croppie('setZoom', zoom);
    });
    $('#upload').on('change', function () {
        //showModal();
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
    $('.cr-slider').on('change', function (ev) {
        if($('#upload').val() != ''){
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                /*popupResult({
                 src: resp
                 });*/
                //console.log(resp);
                $('#base64image').val(resp);
            });
        }

    });

}
$("#resetCropie").click(function () {
    $('#upload-demo').addClass('ready');
    $('#upload').val(''); // this will clear the input val.
    $('#base64image').val();
    $('#picture_upload_div').addClass('abc');
    $('#resetCropie').hide();
    $('.hideOnAction').show();
    $('#imageUploadButton').addClass('disabled');
    $uploadCrop.croppie('bind', {
        url : ''
    }).then(function () {
        console.log('reset complete');
    });
});

$('#showImagePreview').on('click', function (ev) {
    $('#image_uploading').show();
    $('#selected_image_detail').hide();
    $('#selected_image_name').html('');
    setTimeout(function() {
        $('#image_uploading').hide();
        $('#resetCropie').show();
        $('#imageUploadButton').removeClass('disabled');

        $('#picture_upload_div').removeClass('abc');

    }, 1000);
});

function popupResult(result) {
    var html;
    if (result.html) {
        html = result.html;
    }
    if (result.src) {
        html = '<img src="' + result.src + '" />';
    }
    swal({
        title: '',
        html: true,
        text: html,
        allowOutsideClick: true
    });
    setTimeout(function(){
        $('.sweet-alert').css('margin', function() {
            var top = -1 * ($(this).height() / 2),
                left = -1 * ($(this).width() / 2);

            return top + 'px 0 0 ' + left + 'px';
        });
    }, 1);
}

/*
var el = document.getElementById('resizer-demo');
var resize = new Croppie(el, {
    viewport: { width: 100, height: 100 },
    boundary: { width: 300, height: 300 },
    showZoomer: false,
    enableResize: true,
    enableOrientation: true,
    mouseWheelZoom: 'ctrl'
});
resize.bind({
    url: 'demo/demo-2.jpg',
});
//on button click
resize.result('blob').then(function(blob) {
    // do something with cropped blob
});
*/

function demoUpload() {
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) { 
		
            var reader = new FileReader();
            reader.onload = function (e) {
              $('.hideOnAction').hide();
                $('#upload-demo').addClass('ready');
				
				$('#selected_image_detail').show();
				$('#selected_image_name').html(input.files[0].name);
				
                $uploadCrop.croppie('bind', {
                    //zoom:0,
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');

                    $('.cr-slider').attr({'min':0.5000, 'max':1.5000});

                    //$uploadCrop.croppie('setZoom' ,-1);
			

                });
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            console.log("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 200,
            height: 400,
            type: 'square'
        },
        //boundary: { width: 600, height: 600 },
       // enforceBoundary:true,
        enableResize:true,
        enableZoom:true,
        showZoomer:true,
        enableExif: true,
		enableOrientation: true
   // mouseWheelZoom: 'ctrl'
    });
    //
    var zoom = 0;
 //  $uploadCrop.croppie('setZoom', zoom);
	var croppieDetails = $uploadCrop.croppie('get');
	//var zoom = croppieDetails['zoom'];
    $('#zoomPlus').on('click', function () {
        //var croppieDetails = $uploadCrop.croppie('get');console.log(croppieDetails['zoom']);
        zoom = zoom + 0.01; console.log(zoom);
        $uploadCrop.croppie('setZoom', zoom);
       
    });

    $('#zoomMinus').on('click', function () {
         zoom = zoom - 0.01;//console.log(zoom);
        $uploadCrop.croppie('setZoom', zoom); 
    });

    $('#upload').on('change', function () {
        //showModal();
        readFile(this);
    });

    $('.cr-slider').on('change', function (ev) {
        if($('#upload').val() != ''){
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) { console.log(resp);
                $('#base64image').val(resp);
            });
        }

    });
	
	/*$('.cr-slider').on('click', function (ev) {
        if($('#upload').val() != ''){
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) { //console.log(resp);
                $('#base64image').val(resp);
            });
        }

    });*/

    $("#resetCropie").click(function () {
        $('#upload-demo').addClass('ready');
        $('#upload').val(''); // this will clear the input val.
        $('#base64image').val();
        $('#picture_upload_div').hide();
        $('#resetCropie').hide();
		$('.hideOnAction').show();
        $('#imageUploadButton').addClass('disabled');
        $uploadCrop.croppie('bind', {
            url : ''
        }).then(function () {
            console.log('reset complete');
        });
    });
	
	 $('#showImagePreview').on('click', function (ev) {
		$('#image_uploading').show();
		$('#selected_image_detail').hide();
		$('#selected_image_name').html('');
		setTimeout(function() { 
		$('#image_uploading').hide(); 
		$('#resetCropie').show();
        $('#imageUploadButton').removeClass('disabled');
        
		 $('#picture_upload_div').show();
		 	
		}, 1000);
	 });

}



@extends('backend.layouts.master')
@section('content')
    <div class="lyerco_right_table project_inner_table col-md-9 col-sm-9" id="data_table">
        <div class="table_heading_text define_float active-project-heading">
            <h2>{{__('sentence.all_projects.project_management')}}</h2>
			<span>
                <input name="active" type="checkbox" checked class="filterCheckbox"><label class="one">Active</label>
                <input name="in_active"  type="checkbox" checked class="filterCheckbox"><label class="one">Inactive</label>
                <input name="is_archive" type="checkbox" checked class="filterCheckbox"><label class="one">Archive</label>
                <input name="sponsor_signed_off" type="checkbox" checked class="filterCheckbox"><label class="one">Draft</label>
            </span>
        </div>
        <div class="table table-bordered project-management-inner latest-project-status">
            <div class="heading-table">
                <div class="row-table">
                    <div class="th">
                        <p>{!! sorting(__('sentence.all_projects.project_title'), 'project_title', $sortOrder) !!} </p>
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.all_projects.department'), 'department_name', $sortOrder) !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.all_projects.sponsor'), 'sponsor_name', $sortOrder) !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.all_projects.pm'), 'project_manager_name', $sortOrder) !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.all_projects.start'), 'real_start_date', $sortOrder) !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.all_projects.end'), 'realistic_end_date', $sortOrder) !!}
                    </div>
                    <div class="th icon-heading">
                        <img src="{{url('dist/images/clock.png')}}">
                        {!! sorting('', 'time_status', $sortOrder) !!}
                    </div>
                    <div class="th icon-heading">
                        <img src="{{url('dist/images/star.png')}}">
                        {!! sorting('', 'current_status', $sortOrder) !!}
                    </div>
                    <div class="coin th">
                        <img src="{{url('dist/images/coins.png')}}">
                        {!! sorting('', 'cost_status', $sortOrder) !!}
                    </div>
                    <div class="th publish-header">
                        {!! sorting(__('sentence.all_projects.p'), 'is_public', $sortOrder) !!}
                    </div>
                    <div class="th">
                        <p>{{__('sentence.all_projects.sign_off')}}</p>
                    </div>
                </div>
            </div>
            <div class="tbody" id="pagination" >
                @include('backend.all_projects.pagination', $projects)
            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script src="{{url('dist/cropie/cropify_script.js')}}"></script>
    <script>
        demoUpload();
        function openImageModal(id, type, picture){
            //$('#resetCropie').trigger('click');
            $('#element_id').val(id);
            $('#element_type').val(type);
            $('#cropImagePop').modal({backdrop: 'static', keyboard: false});
			 if(picture != '') {
				  $('#pictureSelected').val(picture);
			 }
        }
		
		$( function() {
		 $( ".datepicker" ).datepicker();
	  } );
    </script>
@endsection
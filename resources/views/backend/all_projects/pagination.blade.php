
@foreach($projects as $project)
    @if(isset($project['picture']) and $project['picture'] != null)
        @php $picture = url('image/'.$project['picture']); @endphp
    @else
        @php $picture = url('dist/images/project-img-z.png'); @endphp
    @endif

    <div class="row-table" id="row_{{$project->id}}">
        <div class="td"><a href="{{url('about/project-detail/'.$project->id)}}" target="_blank">{{ $project->project_title }} @if($project->sponsor_signed_off == 0)<span data-title="{!! __('sentence.project_detail_manage.draft_status') !!}"> {!! __('sentence.all_projects.d') !!}</span> @endif</a></div>
        <div class="td">{{ $project->department_name }}</div>
        <div class="td">{{ $project->sponsor_name }}</div>

        <div class="td">{{ $project->project_manager_name }}</div>
        <div class="td">{{ \Carbon\Carbon::parse($project->real_start_date )->format('d/m/y') }}</div>
        <div class="td">{{ \Carbon\Carbon::parse($project->realistic_end_date )->format('d/m/y') }}</div>
        <div class="time td" style="text-align:center;">
            @if($project->time_status >= 1)
                <span class="tooltip"
                      @if($project->time_status=="3")
                      style="background: #9AC31C;"
                      @elseif($project->time_status=="2")
                      style="background: #FFD139;"
                      @elseif($project->time_status=="1")
                      style="background: #ED3535;"
                        @endif
                ></span>
            @endif
             <div class="tooltip-main">
                    <div class="tooltip-header tooltip-heading-one">
                        @if($project->time_status=="3")
                            <img src="{{ asset('dist/images/clock/green-clock.png') }}">
                        @elseif($project->time_status=="2")
                            <img src="{{ asset('dist/images/clock/yellow-clock.png') }}">
                        @elseif($project->time_status=="1")
                            <img src="{{ asset('dist/images/clock/red-clock.png') }}">
                        @endif

                        <h2>{{__('sentence.project_status.time')}}</h2>

                        <div class="tooltip-cross"><img src="{{ asset('dist/images/check-2.png') }}" alt="cross-img"></div>
                    </div>

                    <div class="tooltip-content">
                        <p>
                            @if($project->time_planning_explanation ==null || $project->time_planning_explanation == '')
                                {{ __('sentence.noContent')}}
                            @else
                                {{  substr(strip_tags($project->time_planning_explanation), 0, 350)  }}
                                @if(strlen(strip_tags($project->time_planning_explanation)) > 350)
                                    ... <a href="{{ route('aboutProject',$project->id) }}" target="_blank">{{__('sentence.read_more')}}</a>
                                @endif
                            @endif
                        </p>
                    </div>

                </div>

                <div class="tooltip-overlay"></div>
        </div>

        <div class="time td" style="text-align:center;">
            @if($project->current_status >= 1)
                <span class="tooltip"
                      @if($project->current_status=="3")
                      style="background: #9AC31C;"
                      @elseif($project->current_status=="2")
                      style="background: #FFD139;"
                      @elseif($project->current_status=="1")
                      style="background: #ED3535;"
                        @endif
                ></span>
            @endif                        <!-- Tooltip -->

                <div class="tooltip-main">
                    <div class="tooltip-header tooltip-heading-one">
                        @if($project->current_status=="3")
                            <li><img src="{{ asset('dist/images/star/green-star.png') }}"></li>
                        @elseif($project->current_status=="2")
                            <li><img src="{{ asset('dist/images/star/yellow-star.png') }}"></li>
                        @elseif($project->current_status=="1")
                            <li><img src="{{ asset('dist/images/star/red-star.png') }}"></li>
                        @endif
                        <h2>{{__('sentence.project_status.quality')}}</h2>
                        <div class="tooltip-cross"><img src="{{ asset('dist/images/check-2.png') }}" alt="cross-img"></div>
                    </div>
                    <div class="tooltip-content">
                        <p>
                            @if($project->current_quality_explanation ==null || $project->current_quality_explanation == '')
                                {{ __('sentence.noContent')}}
                            @else
                                {{  substr(strip_tags($project->current_quality_explanation), 0, 350)  }}
                                @if(strlen(strip_tags($project->current_quality_explanation)) > 350)
                                    ... <a href="{{ route('aboutProject',$project->id) }}" target="_blank">{{__('sentence.read_more')}}</a>
                                @endif
                            @endif
                        </p>
                    </div>
                </div>
                <div class="tooltip-overlay"></div>
        </div>

        <div class="time td" style="text-align:center;">
            @if($project->cost_status >= "1")
                <span class="tooltip"
                      @if($project->cost_status=="3")
                      style="background: #9AC31C;"
                      @elseif($project->cost_status=="2")
                      style="background: #FFD139;"
                      @elseif(($project->cost_status=="1"))
                      style="background: #ED3535;"
                        @endif
                ></span>
            @endif
            <div class="tooltip-main">
                <div class="tooltip-header tooltip-heading-one">
                    @if($project->cost_status=="3")
                        <li><img src="{{ asset('dist/images/coin/green-coin.png') }}"></li>
                    @elseif($project->cost_status=="2")
                        <li><img src="{{ asset('dist/images/coin/yellow-coin.png') }}"></li>
                    @elseif($project->cost_status=="1")
                        <li><img src="{{ asset('dist/images/coin/red-coin.png') }}"></li>
                    @endif
                    <h2>{{__('sentence.project_status.cost')}}</h2>
                    <div class="tooltip-cross"><img src="{{ asset('dist/images/check-2.png') }}" alt="cross-img"></div>
                </div>

                <div class="tooltip-content">
                    <p>
                        @if($project->cost_situation_explanation ==null || $project->cost_situation_explanation == '')
                            {{ __('sentence.noContent')}}
                        @else
                            {{  substr(strip_tags($project->cost_situation_explanation), 0, 350)  }}
                            @if(strlen(strip_tags($project->cost_situation_explanation)) > 350)
                                ... <a href="{{ route('aboutProject',$project->id) }}" target="_blank">{{__('sentence.read_more')}}</a>
                            @endif
                        @endif
                    </p>
                </div>
            </div>
            <div class="tooltip-overlay"></div>

        </div>

        <div class="td"> @if($project['is_public'] == 1) {!! __('sentence.all_projects.yes') !!} @else {!! __('sentence.all_projects.no') !!} @endif</div>

        <div class="td">
            @if($project['sponsor_signed_off'] == 1) {{ \Carbon\Carbon::parse($project->sign_off_date)->format('d/m/y') }} @else {!! __('sentence.all_projects.sign_off_pending') !!} @endif
        </div>
    </div>
   
@endforeach

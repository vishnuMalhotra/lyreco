
<!DOCTYPE html>
<html>

<head>
	
	<title>PROPERTIES</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<style type="text/css"> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; }.container {width:100%; display:table; position:relative; height:100px;}</style>

</head>

<body>

	<div class="container" >
		<h2> Active Project's </h2>
		<div class="loader"></div>

		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Project Title</th>
		      <th scope="col">Business Unit</th>
		      <th scope="col">Sponsor</th>
		      <th scope="col">Sponsor Email</th>
		      <th scope="col">Project Manager</th>
		      <th scope="col">Start (est.)</th>
		      <th scope="col">End (est.)</th>
		      <th scope="col">Public</th>
		      <th scope="col">Group</th>
		      <th scope="col">Active</th>
		      <th scope="col">Picture</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
			@if(!empty($all_projects))
				@foreach($all_projects as $index =>$row)
				@if($row->is_active==1)
				    <tr>
				      <th scope="row" id="modifyrow{{$row->id}}" >{{ $index +1 }}</th>
				      <td> {{ $row->project_title }} </td>
				      <td> {{ $row->fk_businessUnitId }} </td>
				      <td> {{ $row->sponsor_name }} </td>
				      <td> {{ $row->sponsor_email }} </td>
				      <td> {{ $row->project_manager }} </td>
				      <td> {{ $row->estimated_start_date }} </td>
				      <td> {{ $row->estimated_end_date }} </td>
				      <td> {{ $row->is_public }} </td>
				      <td> {{ $row->is_group }} </td>
				      <td> {{ $row->is_active }} </td>
				      <td>
				      	@if(!empty($row->picture))
				      		<img src="{{ asset('/') }}{{ $row->picture }}" alt="Girl in a jacket">
				      	@else  No Image  @endif
				      </td>
				      <td>
					    <a href=""> Modify </a>
				      </td>
				    </tr>
				@endif
				@endforeach
			@endif
		    <tr>
		    <form name="ActiveProjectCreate" class="ajax-submit" method="post" action="{{ route('saveProjectBasicDetails') }}" id="ActiveProject_Form" >

		      <th scope="row">##</th>

		      <td>
		      	<input type="text" name="project_title" id="project_title" value="Just For Test" required="" >
		      </td>

		      <td>
		      	<select name="fk_businessUnitId" id="fk_businessUnitId" required="" >
		      		@if(!empty($all_business_unit))
		      			@foreach($all_business_unit as $unit )
		      				@if($unit->is_hidden==1)
		      					<option value="{{ $unit->id }}" > {{ $unit->department_name }} </option>
		      				@endif
		      			@endforeach
		      		@endif
		      	</select>

		      </td>

		      <td>
		      	<input type="text" name="sponsor_name" id="sponsor_name" value="" >
		      </td>

		      <td>
		      	<input type="email" name="sponsor_email" id="sponsor_email" value="" required="" >
		      </td>

		      <td>
		      	<select name="project_manager" id="project_manager" required="true" >
		      		<option value="1" > Manager 1 </option>
		      		<option value="2" > Manager 2 </option>
		      	</select>
		      </td>

		      <td>
		      	<input type="date" name="estimated_start_date" value="" id="estimated_start_date" required="" >
		      </td>

		      <td>
		      	<input type="date" name="estimated_end_date" value="" id="estimated_end_date" required="" >
		      </td>

		      <td>
		      	<input type="checkbox" value="1" name="is_public" id="is_public" >
		      </td>

		      <td>
		      	<input type="checkbox" value="1" name="is_group" id="is_group" >
		      </td>

		      <td>
		      	<input type="checkbox" value="1" name="is_active" id="is_active" >
		      </td>

		      <td>
		      	<input type="file" name="picture" id="picture" accept="image/x-png,image/gif,image/jpeg" >
		      </td>

		      <td>
		      	<input type="submit" name="submit" value="ADD" >
		      </td>

		    </form>

		    </tr>
		  </tbody>

		</table>
	</div>





		<div class="container" >
		<h2> In Active Project's ( ARCHIVED ) </h2>
		<div class="loader"></div>

		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Project Title</th>
		      <th scope="col">Business Unit</th>
		      <th scope="col">Sponsor</th>
		      <th scope="col">Sponsor Email</th>
		      <th scope="col">Project Manager</th>
		      <th scope="col">Start (est.)</th>
		      <th scope="col">End (est.)</th>
		      <th scope="col">Public</th>
		      <th scope="col">Group</th>
		      <th scope="col">Active</th>
		      <th scope="col">Picture</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
			@if(!empty($all_projects))
				@foreach($all_projects as $index =>$row)
				    @if($row->is_active==0)
				    <tr>
				      <th scope="row" id="modifyrow{{$row->id}}" >{{ $index +1 }}</th>
				      <td> {{ $row->project_title }} </td>
				      <td> {{ $row->fk_businessUnitId }} </td>
				      <td> {{ $row->sponsor_name }} </td>
				      <td> {{ $row->sponsor_email }} </td>
				      <td> {{ $row->project_manager }} </td>
				      <td> {{ $row->estimated_start_date }} </td>
				      <td> {{ $row->estimated_end_date }} </td>
				      <td> {{ $row->is_public }} </td>
				      <td> {{ $row->is_group }} </td>
				      <td> {{ $row->is_active }} </td>
				      <td>
				      	@if(!empty($row->picture))
				      		{{ $row->picture }}
				      	@else  No Image  @endif
				      </td>
				       <td>
					      	<a href=""> Modify </a>
					      	|
					      	<a data-url="{{ route('deleteProjectBasicDetails') }}" data-id="{{ $row->id }}" class="__drop" > Delete </a>
				        </td>
				    </tr>
				    @endif
				@endforeach
			@endif
		  </tbody>

		</table>
	</div>


<script src="{{ asset('js/custom/script.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	function get_alert(){
		swal("Are you sure?", {
		  dangerMode: true,
		  buttons: true,
		  confirmButtonText: "Yes!",
		});
	}
	function return_msg($type="success",$title="Good job!", $message="You clicked the button!"){
		swal($title, $message, $type).then(function(){ location.reload(); });
	}

</script>

</body>
</html>

</script>

@extends('backend.layouts.master')
@section('content')

    <div class="lyerco_right_table project_inner_table col-md-9 col-sm-9 ">
        <div class="table_heading_text define_float active-project-heading">
            <h2>{{__('sentence.sponsored_project.project_management')}}</h2>
            @if($setting['sponsor_can_create_projects'] == 1)
                <span>
                    <a class="active-heading" href="{{ url('admin/sponsor/project/add') }}">{{__('sentence.project_manage.create_new_project')}}</a>
                </span>
            @endif
        </div>

        <div class="table table-bordered project-management-inner sponsored-project-new">
            <div class="heading-table">
                <div class="row-table">
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.projects'), 'project_title', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.department'), 'department_name', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.project_manager'), 'project_manager_name', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.latest_update'), 'project_status_updated_at', $sortOrder ?? '') !!}
                    </div>
                    <div class="th icon-heading">
                        <img src="{{url('dist/images/clock.png')}}">
                        {!! sorting('', 'time_status', $sortOrder ?? '') !!}
                    </div>
                    <div class="th icon-heading">
                        <img src="{{url('dist/images/star.png')}}">
                        {!! sorting('', 'current_status', $sortOrder ?? '') !!}
                    </div>
                    <div class="coin th">
                        <img src="{{url('dist/images/coins.png')}}">
                        {!! sorting('', 'cost_status', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.public'), 'is_public', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.group'), 'is_group', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        {!! sorting(__('sentence.sponsored_project.active'), 'is_active', $sortOrder ?? '') !!}
                    </div>
                    <div class="th">
                        <p>{!! __('sentence.sponsored_project.pic') !!}</p>
                    </div>
                    <div class="th">
                        <p>{{__('sentence.sponsored_project.action')}}</p>
                    </div>
                </div>
            </div>

            <div class="tbody" id="pagination">
                @include('backend.sponsored_projects.pagination', $all_projects)
            </div>

        </div>
        <div class="update-btn td spons-note">
            <span class="pull-left">{!! __('sentence.sponsored_project.note') !!}</span>
            <a href="{{route('allArchivedProjects')}}"  class="pull-right" target="_blank">{{__('sentence.sponsored_project.show_archived')}}</a>
        </div>
    </div>
@endsection
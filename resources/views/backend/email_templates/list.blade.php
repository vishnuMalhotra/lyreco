@extends('backend.layouts.master')
@section('content')
    <div class="lyerco_right_table business-unit col-md-9 col-sm-9 col-xs-9"  id="data_table">
        <div class="table_heading_text define_float">
            <h2>{{ __('sentence.email_template_lang.email_template')}}</h2>
        </div>
        <div class="table table-bordered table-business">
            <div class="heading-table">
                <div class="row-table">
                    <div class="th" style="width:auto !important;">
                        #
                    </div>
                    <div class="th">
                        {{ __('sentence.email_template_lang.code')}}
                    </div>
                    <div class="th">
                        {{ __('sentence.email_template_lang.name')}}
                    </div>
                    <div class="th">
                        {{ __('sentence.email_template_lang.subject')}}
                    </div>
                    <div class="th">
                        {{ __('sentence.email_template_lang.view')}}
                    </div>
                </div>
            </div>
            <div class="tbody" id="pagination">
                @foreach($emailTemplates as $key =>$emailTemplate)
                    <div class="row-table" >
                        <div class="td">{{$key+1}}</div>
                        <div class="td">{!! $emailTemplate['template_code'] !!}</div>
                        <div class="td">{!! $emailTemplate['template_name'] !!}</div>
                        <div class="td">{!! $emailTemplate['template_subject'] !!}</div>
                        <div class="td"><a href="{{route('template_detail', $emailTemplate['id'])}}">{{ __('sentence.email_template_lang.view_details')}} </a> </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
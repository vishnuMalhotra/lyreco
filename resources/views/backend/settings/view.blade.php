@extends('backend.layouts.master')
@section('content')
    <div class="lyerco_right_table project_inner_table col-md-9 col-sm-9 basic-settings">
        {{Form::model($settings, array('url'=>route('saveSettings'), 'class'=>'row-table ajax-submit','enctype'=>'multipart/form-data', 'id'=>'profile_edit'))}}
            <div class="project-header-main">
                <div class="project-heading-list  profile-list">
                    <div class="my-profile define_float">
                        <h2>{!! __('sentence.settings_title') !!}</h2>
                    </div>
                </div>
            </div>
            <div class="project-details-main my-profile-inner">
                <h2>{!! __('sentence.settings.general_settings_title') !!}</h2>
                <div class="project-heading-list col-md-8 profile-list">
                    <ul class="password-manage-list">
                        <li><span class="profile-left progile-lang">{!! __('sentence.settings.standard_language') !!}</span>
                            <div class="select-lang-inner select-lang">{{Form::select('fallback_language',languageCollector(), null, array('class'=>'text-field'))}}</div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="project-details-main my-profile-inner">
                <h2>{!! __('sentence.settings.link_settings_title') !!}</h2>
                <div class="project-heading-list col-md-12 profile-list">
                    <ul class="password-manage-list">
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.guest_autologin_link') !!}</span>
                            http://lprt.lyreco.swiss/{{Form::text('guest_login_link',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.guest_autologin_link_placeholder')))}}
                        </li>
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.talent_link') !!}</span>
                            {{Form::text('talented_team_members',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.talent_link_placeholder')))}}
                        </li>
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.resources_link') !!}</span>
                            {{Form::text('pm_resource_base_url',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.resources_link_placeholder')))}}
                        </li>
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.user_manual_link') !!}</span>
                            {{Form::text('user_manual_link',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.user_manual_link_placeholder')))}}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="project-details-main my-profile-inner">
                <h2>{!! __('sentence.settings.email_settings_title') !!}</h2>
                <div class="project-heading-list col-md-12 profile-list">
                    <ul class="password-manage-list">
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.auto_reminder_email_activated') !!}</span>
                            {{Form::checkbox('activate_email_autoreminder',null, null,array('class'=>'text-field'))}}
                        </li>
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.reminder_email_threshold') !!}</span>
                            {{Form::number('reminder_email_threshold_days',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.reminder_email_threshold_placeholder'), 'min'=>0))}}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="project-details-main my-profile-inner">
                <h2>{!! __('sentence.settings.project_settings_title') !!}</h2>
                <div class="project-heading-list col-md-12 profile-list">
                    <ul class="password-manage-list">
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.new_icon_threshold') !!}</span>
                            {{Form::number('new_icon_threshold',null,array('class'=>'text-field','placeholder'=>__('sentence.settings.new_icon_threshold_placeholder'), 'min'=>0))}}
                        </li>
                        <li>
                            <span class="profile-left progile-lang">{!! __('sentence.settings.sponsor_can_create_projects') !!}</span>
                            {{Form::checkbox('sponsor_can_create_projects',null, null,array('class'=>'text-field'))}}
                        </li>
                    </ul>
                </div>
            </div>
            <button class="my-profile-btn " type="submit" >{{__('sentence.settings.save')}}</button>
        {{Form::close()}}
    </div>
@endsection
@extends('backend.layouts.master')
<style type="text/css"> #options li { line-height:25px; color:blue; cursor:pointer; }.border-red-error{ background: #fff !important; } .border-red{ background: #fdeaea; } </style>

@section('content')
	<div class="h2_tool">
		<span class="tooltip"></span>
		<div class="tooltip-mobile">
			<div class="tooltip-main" id="tooltipMain" style="display: none;">
				<div class="tooltip-header">
					<div class="tooltip-cross"><img src="{{ asset('dist/images/check-2.png') }}" alt="cross-img"></div>
				</div>
				<div class="tooltip-content">
					<p  id="tooltip_content"> </p>
				</div>
			</div>
			<div class="tooltip-overlay"></div>
		</div>
	</div>
			<div class="lyerco_right_table business-unit col-md-9 col-sm-9 project-a">
				<div class="table_heading_text define_float">
					<h2>{{$project->project_title}} @if($project->sponsor_signed_off == 0) {!! __('sentence.project_detail_manage.draft') !!} @endif</h2>
				</div>
				<div class="project-header-main">
					<div class="project-heading-list col-md-7">
						<ul>
							<li><h6>{{__('sentence.project_detail_manage.project_title')}}:</h6><span>{{$project->project_title}}</span></li>
							<li><h6>{{__('sentence.project_detail_manage.department')}}:</h6><span>{{$project->businessName}}</span></li>
							<li><h6>{{__('sentence.project_detail_manage.project_manager')}}:</h6><span>{{$projectManager}}</span></li>
							<li><h6>{{__('sentence.project_detail_manage.project_sponsor')}}:</h6><span>{{$project->sponsor_name}}</span></li>
							<li><h6>{{__('sentence.project_detail_manage.sponsor_signoff')}}:</h6><span>@if($project->sponsor_signed_off == 1) {{ \Carbon\Carbon::parse($project->sign_off_date)->format('d-m-Y') }} @else {!! __('sentence.project_detail_manage.sponsor_signoff_pending') !!} @endif</span></li>
						</ul>
					</div>
					<div class="project-heading-list col-md-5">
						<ul>
							<li><h6>{{__('sentence.project_detail_manage.public')}}:</h6><span>@if($project->is_public==1) {{__('sentence.project_detail_manage.yes')}} @else {{__('sentence.project_detail_manage.no')}} @endif</span></li>
							<li><h6>{{__('sentence.project_detail_manage.group')}}:</h6><span>@if($project->is_group==1) {{__('sentence.project_detail_manage.yes')}} @else {{__('sentence.project_detail_manage.no')}} @endif</span></li>
							<li><h6>{{__('sentence.project_detail_manage.active')}}:</h6><span>@if($project->is_active==1) {{__('sentence.project_detail_manage.yes')}} @else {{__('sentence.project_detail_manage.no')}} @endif</span></li>
							<li><h6>{{__('sentence.project_detail_manage.planned_start')}}:</h6><span>{{ \Carbon\Carbon::parse($project->estimated_start_date)->format('d/m/y') }}</span></li>
							<li><h6>{{__('sentence.project_detail_manage.planned_end')}}:</h6><span>{{ \Carbon\Carbon::parse($project->estimated_end_date)->format('d/m/y') }}</span></li>
						</ul>
					</div>
				</div>
 
				<!-- <form name="ActiveProjectCreate" class="ajax-submit" method="post" action="{{ route('updateProjectDetails') }}" id="ActiveProject_Form" > -->
			{{Form::model($project, array('url'=>route('updateProjectDetails'), 'class'=>'row-table ajax-submit', 'id'=>'project_detail','files' => true))}}

				<div class="project-details-main">
					<h2>{{__('sentence.project_detail_manage.project_details')}}
						@if(__('sentence.project_detail_manage.project_details_tooltip') != '' )
						    <sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.project_details_tooltip') !!}"></i></sup>
						@endif
					</h2>
					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.project_description_publicly_available')}}
							@if(__('sentence.project_detail_manage.project_description_publicly_available_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.project_description_publicly_available_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('project_description', $project->project_description,array('placeholder'=>__('sentence.project_detail_manage.project_description_publicly_available_placeholder')))}}
						{{Form::hidden('id', $project->id)}}
						{{Form::hidden('project_title', $project->project_title)}}
						{{Form::hidden('fk_businessUnitId', $project->fk_businessUnitId)}}
						{{Form::hidden('project_manager', $project->project_manager)}}
						{{Form::hidden('sponsor_name', $project->sponsor_name)}}
						{{Form::hidden('is_public', $project->is_public)}}
						{{Form::hidden('is_group', $project->is_group)}}
						{{Form::hidden('is_active', $project->is_active)}}
						{{Form::hidden('estimated_start_date', $project->estimated_start_date)}}
						{{Form::hidden('estimated_end_date', $project->estimated_end_date)}}
						{{Form::hidden('sponsor_email', $project->sponsor_email)}}
					</div>

					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.current_situation')}}
							@if(__('sentence.project_detail_manage.current_situation_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.current_situation_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('current_situation', $project->current_situation,array('placeholder'=>__('sentence.project_detail_manage.current_situation_placeholder')))}}
					</div>

					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.project_objective')}}
							@if(__('sentence.project_detail_manage.project_objective_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.project_objective_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('project_objective', $project->project_objective,array('placeholder'=>__('sentence.project_detail_manage.project_objective_placeholder')))}}
					</div>

					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.prerequisites_dependencies_and_exclusions')}}
							@if(__('sentence.project_detail_manage.prerequisites_dependencies_and_exclusions_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.prerequisites_dependencies_and_exclusions_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('prerequisite_dependencies_exclusions', $project->prerequisite_dependencies_exclusions,array('placeholder'=>__('sentence.project_detail_manage.prerequisites_dependencies_and_exclusions_placeholder')))}}
					</div>
					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.alternatives_options')}}
							@if(__('sentence.project_detail_manage.alternatives_options_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.alternatives_options_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('alternative_or_options', $project->alternative_or_options,array('placeholder'=>__('sentence.project_detail_manage.alternatives_options_placeholder')))}}
					</div>
					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.milestones')}}
							@if(__('sentence.project_detail_manage.milestones_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.milestones_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('milestones', $project->milestones,array('placeholder'=>__('sentence.project_detail_manage.milestones_placeholder')))}}
					</div>
					<div class="project-textarea">
						<p>{{__('sentence.project_detail_manage.required_resources')}}
							@if(__('sentence.project_detail_manage.required_resources_tooltip') != '' )
								<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.required_resources_tooltip') !!}"></i></sup>
							@endif
						</p>
						{{Form::textarea('required_resources', $project->required_resources,array('placeholder'=>__('sentence.project_detail_manage.required_resources_placeholder')))}}
					</div>
				</div>

				<div id="projectMemversListing" >
					 @include('backend.myproject.doc_members',array('projectId'=>$project->id))
				</div>
				<div class="project-tagsarea setting_right">
					<div class="edit_form_right col-md-1 col-sm-1 col-xs-2">
						<div class="edit_cus_check">
							{{Form::checkbox('request_for_team_member', null)}}
							<label></label>
						</div>
					</div>
					<div class="edit_form_right col-md-11 col-sm-11 col-xs-10 setting-tag">
						{!! __('sentence.project_detail_manage.request_for_team_member') !!}
					</div>
				</div>

				<div class="project-details-main " >
					<h2>{{__('sentence.project_detail_manage.links')}}
						@if(__('sentence.project_detail_manage.add_link_tooltip') != '' )
							<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.add_link_tooltip') !!}"></i></sup>
						@endif
					</h2>
					<div class="project-tagsarea links-div">
						<div class="add-tags add-tag">
							<div class="file-upload upload-file-tag">
								<a class="add-tags-btn" data-toggle="modal" data-target="#add_link">{{__('sentence.project_detail_manage.add')}}</a>
							</div>
						</div>
						<div id="links_div"></div>
						@if(!empty($projectLinks))
							@foreach($projectLinks as $projectLink)
								<div class="add-tags removeMaindiv project-tagsarea">
									<p>
										<input name="linkId[]" type="hidden" value="{{$projectLink['id']}}">
										<input name="title[]" type="hidden" class="link_title" value="{{$projectLink['title']}}">
										<input type="hidden" name="url[]" class="full_url" value="{{$projectLink['url']}}">
										<input type="hidden" name="url_is_public[]" value="{{$projectLink['is_public']}}">

										<a href="{{$projectLink['url']}}" target="_blank">
											<span class="tag-text">{{$projectLink['title']}} </span>
										</a>
										@if($projectLink['is_public'] == 0)
											<img class="block-image" src="{{ url('dist/images/block.png') }}">
										@endif
										<span class="removeItem">
											<img src="{{url('dist/images/check-2.png')}}">
										</span>
									</p>
								</div>
							@endforeach
						@endif
					</div>
				</div>

				<div class="project-details-main project-members">
					<h2>{{__('sentence.project_detail_manage.add_public_documents')}}
						@if(__('sentence.project_detail_manage.add_public_documents_tooltip') != '' )
							<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.add_public_documents_tooltip') !!}"></i></sup>
						@endif
					</h2>
					<div class="project-tagsarea">
						<div class="add-tags add-tag">
							<div class="file-upload upload-file-tag">
								<input name="public_documents[]" type="file" id="public_documents" data-preview="public_documents_div" multiple  class="tags-field" >
								<span for="public_documents" class="add-tags-btn">{{__('sentence.project_detail_manage.add')}}</span>
							</div>
						</div>
						<div id="public_documents_div" data-id="public_documents"></div>
						@if(!empty($projectDocument))
							@foreach($projectDocument as $document)
								@if($document->is_public==1)
									<div class="add-tags removeMaindiv">
										<p> <a href="{{ route('downloadFile',$document->id) }}">{{$document->document}}</a>  &nbsp&nbsp&nbsp&nbsp<span class="removeDoc" data-id="{{$document->id}}" route="{{route('remove.document')}}" data-table="document"><img src="{{ asset('dist/images/check-2.png') }}"></span></p>
									</div>
								@endif
		                    @endforeach
		                @endif
					</div>
				</div>


				<div class="project-details-main project-members">
					<h2>{{__('sentence.project_detail_manage.add_internal_documents')}}
						@if(__('sentence.project_detail_manage.add_internal_documents_tooltip') != '' )
							<sup><i class="fa fa-question-circle tooltip-icon" aria-hidden="true" data-tooltip-content="{!! __('sentence.project_detail_manage.add_internal_documents_tooltip') !!}"></i></sup>
						@endif
					</h2>
					<div class="project-tagsarea">
						<div class="add-tags add-tag" >
							<div class="file-upload upload-file-tag">
								<input type="file" id="internal_documents" name="internal_documents[]" multiple="true"  data-preview="internal_documents_div" class="tags-field" />
								<span for="internal_documents" class="add-tags-btn">{{__('sentence.project_detail_manage.add')}}</span>
							</div>
						</div>
						<div id="internal_documents_div" data-id="internal_documents"></div>
						@if(!empty($projectDocument))
							@foreach($projectDocument as $document)
								@if($document->is_public!=1)
									<div class="add-tags removeMaindiv">
										<p><a href="{{ route('downloadFile',$document->id) }}">{{$document->document}}</a> &nbsp&nbsp&nbsp&nbsp<span class="removeDoc" route="{{route('remove.document')}}" data-id="{{$document->id}}" data-table="document"><img src="{{ asset('dist/images/check-2.png') }}"></span></p>
									</div>
								@endif
		                    @endforeach
		                @endif
					</div>
				</div>

				@if($project->sponsor_signed_off == 0)
					@if($project->fk_sponsorId == \Auth::user()->id)
						<div class="project-details-main project-members" >
							<div class="project-tagsarea setting_right">
								<div class="edit_form_right col-md-1 col-sm-1 col-xs-2">
									<div class="edit_cus_check">
										{{Form::checkbox('sponsor_signed_off', null,null, array( 'id'=>'sponsor_signed_off'))}}
										<label></label>
									</div>
								</div>
								<div class="edit_form_right col-md-11 col-sm-11 col-xs-10 setting-tag">
									{!! __('sentence.project_detail_manage.i_approve_project_details') !!}
								</div>
							</div>
							<p>{!! __('sentence.project_detail_manage.signoff_note') !!}</p>
						</div>
					@else
						<div class="project-details-main project-members" >
							<p>{!! __('sentence.project_detail_manage.project_manager_signed_off_note') !!}</p>
						</div>
					@endif
				@else
					<p>{!! __('sentence.project_detail_manage.signedoff_note', ['date'=> \Carbon\Carbon::parse($project['sign_off_date'])->format('d-m-Y'), 'user'=>$project['signedOffBy'] ]) !!}</p>
				@endif

				@if($project->current_situation == null || $project->current_situation == '')
					<input type="hidden" name="change_reason" value="sentence.initial_filling">
				@else
					<div class="modal fade delete-modal" id="confirm_change_reason" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content reminder-modal">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">×</span> </button>
									<h5 class="modal-title">{!! __('sentence.change_confirmation') !!}</h5>
								</div>
								<div class="modal-body">
									<h4>{!! __('sentence.change_confirmation_description') !!}</h4>
									<div class="edit_form_right ">
										<input type="text" class="text-field" id="project_change_reason" placeholder="{!! __('sentence.change_reason_placeholder') !!}" name="change_reason" maxlength="60">
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary pop-btn" >{{ __('sentence.save_change_reason')}}</button>
								</div>
							</div>
						</div>
					</div>
				@endif
		    	<button type="submit" class="save-deatils">{{__('sentence.project_detail_manage.save')}}</button>

				
		    {{Form::close()}}
			@if(count($projectHistory) > 0)
				<div class="about-content about-details">
					<div class="about-content-inner about-history-new about-details-inner">
						<h2>{!! __('sentence.change_history') !!}</h2>
						@foreach($projectHistory->take(3) as $history)
							 <p>
								<a href="{{route('projectHistory', $history['id'])}}" target="_blank">{{\Carbon\Carbon::parse($history['created_at'])->timezone(\Cookie::get('timezone'))->format('d-m-Y h:i:s')  }} 
												
													{!!   __( $history['change_reason'] ) !!}
												
											{{ ' ('. __('sentence.edited_by').' '.$history['updated_by'].') ' }}
								</a>
							 </p>
						@endforeach
					</div>
				</div>
				<div class="about-details about-details-second">
					<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
						<div class="about-content-inner about-history-new">
							@foreach($projectHistory->skip(3) as $history)
								<p>
									<a href="{{route('projectHistory', $history['id'])}}" target="_blank">{{\Carbon\Carbon::parse($history['created_at'])->timezone(\Cookie::get('timezone'))->format('d-m-Y h:i:s') .' - '.$history['change_reason'].' ('. __('sentence.edited_by').' '.$history['updated_by'].') ' }}</a>
								</p>
							@endforeach
						</div>
					</div>
					@if(count($projectHistory) > 3)
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								<span>+</span><span class="minus-icon"></span>{{ __('sentence.show_older_items')}}
							</a>
						</div>
					@endif
				</div>
			@endif
	</div>
	<div class="modal fade add-link" id="add_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content reminder-modal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">×</span> </button>
					<h5 class="modal-title">{!! __('sentence.project_detail_manage.add_link_heading') !!}</h5>
				</div>
				<div class="modal-body">
					<div class="project-details-main my-profile-inner">
						<div class="add-link-modal">
							<div class="add-link-feild">
								<div class="col-md-3"><span class="profile-left">{!! __('sentence.project_detail_manage.link_title') !!}</span></div>
								<div class="col-md-9 edit_form_right ">
									<input class="text-field" type="text" maxlength="50"  max="50" name="test" placeholder="{!! __('sentence.project_detail_manage.link_title_placeholder') !!}" autocomplete="off" id="link_title">
								</div>
							</div>
							<div class="add-link-feild">
								<div class="col-md-3"><span class="profile-left">{!! __('sentence.project_detail_manage.url') !!}</span></div>
								<div class="col-md-9 edit_form_right ">
									<input class="text-field" type="text" placeholder="{!! __('sentence.project_detail_manage.url_placeholder') !!}"  id="full_url" autocomplete="off">
								</div>
							</div>
							<div class="add-link-feild">
								<div class="col-md-3"><span class="profile-left">{!! __('sentence.project_detail_manage.is_public') !!}</span></div>
								<div class="col-md-9 add-links-check edit_form_right">
									<div class="edit_cus_check">
									<input name="is_public" type="checkbox" id="url_is_public" autocomplete="off">
									<label></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pop-btn" onClick="addLink()">{{ __('sentence.project_detail_manage.save_link')}}</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('body').click(function() {
	   $('.membersName').hide();
	});

	function addLink() {
		var public = '';
		var not_public = '<img class="block-image" src="{{ url('dist/images/block.png') }}">';

		$('#link_title').removeClass('border-red');
		$('#full_url').removeClass('border-red');

		var url = $('#full_url').val();
		var title = $('#link_title').val();

		if(title == "" && url == "") {
			$('#link_title').addClass('border-red');
			$('#full_url').addClass('border-red');
			return false;
		}

		if(title == "") {
			$('#link_title').addClass('border-red');
			return false;
		}
		if(url == "") {
			$('#full_url').addClass('border-red');
			return false;
		}


		var title_found = 0;
		$(".links-div .removeMaindiv .link_title").each(function( index ) {
			if(this.value.toLowerCase() == title.toLowerCase()) {
				title_found = 1;
			}
		});
		if(title_found == 1) {
			$('#link_title').addClass('border-red');
			return false;
		}
		var found = 0;

		$(".links-div .removeMaindiv .full_url").each(function( index ) {
			if(this.value.toLowerCase() == url.toLowerCase()) {
				found = 1;
			}
		});
		if(found == 1) {
			$('#full_url').addClass('border-red');
			return false;
		}
		var checked = 0;
		var isPublic = not_public;
		if($('#url_is_public').is(':checked')) {
			checked = 1;
			isPublic = public;
		}



		if(url != "") {
			var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
			if(!pattern.test(url)) {
				$('#full_url').addClass('border-red');
				return false;
			}

			var ItemDetail =  '<div class="add-tags removeMaindiv project-tagsarea">' +
									'<p>' +
										'<input name="linkId[]" type="hidden" value="0">' +
										'<input name="title[]" type="hidden" class="link_title" value="'+title+'">' +
										'<input type="hidden" name="url[]" class="full_url" value="'+url+'">' +
										'<input type="hidden" name="url_is_public[]" value="'+checked+'">' +
										'<a href="'+url+'" target="_blank"><span class="tag-text">' +title +"</span></a>"+isPublic  +
										'<span class="removeItem">' +
											'<img src="{{ asset('dist/images/check-2.png') }}">' +
										'</span>' +
									'</p>' +
							   '</div>';
			$(".links-div").append(ItemDetail);
			$('#add_link').modal('hide');
			$('#full_url').val('');
			$('#link_title').val('');
			$("#url_is_public").prop('checked', false);
		}
	}
	
	  $(document).on("click",".removeItem",function() {
            $(this).closest('.removeMaindiv').remove();
        });

	
	$("#internal_documents, #public_documents").change(function(){
		var preview = $(this).attr('data-preview');
		var file_field_name = $(this).attr('name');
        images(this, '#'+preview, file_field_name);
    });

	function removePic(li, i){
		var fileFiledId= $(li).parent().parent('div').parent('div').attr('data-id');
		$(li).parent('p').parent('div').remove();
		$("#"+fileFiledId).val("");
	}

    var images = function(input, imgPreview, file_field_name) {// console.log(input);
		var allFiles = input.files;
		if (allFiles) {
			var fd = new FormData();
			var files = allFiles;
			var totalFiles = allFiles.length;
			for (var index = 0; index < totalFiles; index++) {
				fd.append("documents[]", allFiles[index]);
			}

			$.ajax({
				url: "{{ route('documentUpload') }}",
				type: 'post',
				data: fd,
				contentType: false,
				processData: false,
				beforeSend: function() {
                	showLoader();
            	},
				success: function(response){
					hideLoader();
					console.log(response);
					$.each(response, function(i, v) {
						var html = '<div class="add-tags removeMaindiv">' +
								        '<input type="hidden" name="hidden_'+file_field_name+'" value="'+v+'">'+
										'<p> '+ v +' &nbsp&nbsp&nbsp&nbsp  <span  onclick="removePic(this, '+i+')">' +
										'<img src="'+window.location.origin+'/dist/images/check-2.png"></span></p>'+
									'</div>';
						$($.parseHTML(html)).appendTo(imgPreview);
					});

				}
			});
			$(input).val("");
		}
    };

    $('.membersName').hide();
		$(document).on("click","input[type='text']",'#projectMembers', function(){
    	$('.membersName').show();
	});

 
    function addToMember(){
		var route = "{{ route('addToMembers') }}";
		var project_id = "{{$project->id}}";
		var memberName = $('#projectMembers').val();
		var inputVal = document.getElementById("projectMembers").value;
		
		if(inputVal != ''){
			/*$(".add-members-todiv").append('<div class="add-tags removeMaindiv"><p> '+inputVal+' &nbsp&nbsp&nbsp&nbsp <span class="removeMem"   route="{{route('remove.document')}}"><img src="{{ asset('dist/images/check-2.png') }}"></span></p></div>');*/
			$.ajaxSetup({
	        	headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        	}
	    	}); 
	    	$.post(
	        	route,
	        	{project_id:project_id,memberName:memberName},
	        	function (data) {
		            $("#projectMemversListing").html(data);
		            $('.membersName').hide();
	        	}
	    	);
			$("#projectMembers").val('');
		}
		event.preventDefault();
	}

	$(document).on("click",".removeMem",function() {
	    var table = $(this).attr('data-table');
	    var project_id = "{{$project->id}}";
	    var id = $(this).attr('data-id');
	    var route =$(this).attr('route');
	    $(this).closest('.removeMaindiv').remove();
	    $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.post(
	        route,
	        {table: table, id: id,project_id:project_id},
	        function (data) {
	            $("#projectMemversListing").html(data);
	        }
	    );
	});

	/*function updateProjectChangeReason() {
		if ($("#sponsor_signed_off").is(":checked")) {
			$('#project_change_reason').val('sentence.project_details_approved');
		} else{
			$('#project_change_reason').val('');
		}
	}*/
</script>
<style>
	.tooltip-mobile.tooltip_open {     display: block; }
</style>

<script>
	//mobile
	$(".tooltip-cross").click(function(){
		$("body").removeClass("tooltip-body_main");
		$('.tooltip-mobile').removeClass('tooltip_open');
	});
	$(".tooltip-icon").click(function() { 
		var content = $(this).attr('data-tooltip-content');
		$('#tooltip_content').html(content);
		$("body").addClass("tooltip-body_main");
		$('.tooltip-mobile').addClass('tooltip_open');

	});

	$(document).ready(function(){
		//web
		$('body').on('mouseleave','.h2_tool', function() {
			$('.tooltip-main').hide();
		});
		$('body').on('mouseleave','.project-details-main', function() { console.log('called');
			//$('.tooltip-main').hide();
		});
			$('body').on('mouseleave','textarea', function() { console.log('called');
			$('.tooltip-main').hide();
		});

		$('body').on('mouseenter', '.project-header-main', function () {
			$('.tooltip-main').hide();
		})

		$('body').on('mouseenter', '.tooltip-icon', function (event) { 
			var content = $(this).attr('data-tooltip-content');
			$('#tooltip_content').html(content);
			$('.tooltip-main').show();
			$(".h2_tool").css({top: $(document).scrollTop() + event.clientY , left: event.clientX, 'position':'absolute'}).show();
		});
	});
</script>
@endsection


<!DOCTYPE html>
<html>

<head>
	<title>testing</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

		<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

	</style>
</head>

<body>

	<div class="container" >
		<h2> User Management </h2>

		<div class="loader"></div>

		<table class="table">
		  
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">User Name</th>
		      <th scope="col">Email Addess</th>
		      <th scope="col">User Role</th>
		      <th scope="col">Active Status</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
			@if(!empty($all_users))  	
				@foreach($all_users as $index =>$row)
				    <tr>
				      <th scope="row">{{ $index +1 }}</th>
				      <td>{{ $row->name }}</td>
				      <td>{{ $row->email }}</td>
				      <td>{{ $row->role }}</td>
				      <td>{{ $row->is_Active }}</td>
				      <td>
				      	<a href=""> modify </a> | <a data-url="{{ route('deleteUsersDetail') }}" data-id="{{ $row->id }}" class="__drop" > delete </a>
				      </td>
				    </tr>
				@endforeach
			@endif
		    <tr>
		    <form name="roleCreate" class="ajax-submit" method="post" action="{{ route('createUserDetail') }}" id="rollsform" >

		      <th scope="row">##</th>
		      <td>
		      	<input type="text" name="name" id="name" >
		      </td>
		      <td>
		      	<input type="text" name="email" id="email" >
		      </td>
		      <td>
		      	<select name="role" required="" id="role" >		
		      		@if(!empty($roles))
		      			@foreach($roles as $role)
		      				<option value="{{ $role->id }}" > {{ $role->name }} </option>
		      			@endforeach
		      		@endif
		      	</select>
		      </td>
		      <td>
		      	<select name="is_Active" required="" id="is_Active" >
		      		<option value="1" selected > Active </option>
		      		<option value="0" > Un-Active </option>
		      	</select>
		      </td>
		      <td>
		      	<input type="submit" name="submit" value="Create" >
		      </td>

		    </form>

		    </tr>
		  </tbody>

		</table>
	</div>


<script src="{{ asset('js/custom/script.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	function get_alert(){
		swal("Are you sure?", {
		  dangerMode: true,
		  buttons: true,
		  confirmButtonText: "Yes!",
		});
	}
	function return_msg($type="success",$title="Good job!", $message="You clicked the button!"){
		swal($title, $message, $type).then(function(){ location.reload(); });
	}

</script>

</body>
</html>

</script>

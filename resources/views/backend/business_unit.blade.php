<!DOCTYPE html>
<html>
<head>
	<title>testing</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	
	<style type="text/css"> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; } </style>

</head>

<body>

	<div class="container" >
		<h2> Business Unit </h2>
		<div class="loader"></div>
		<form name="businessUnitCreate" class="ajax-submit" method="post" action="{{ route('createBusinessUnit') }}" id="businessUnitCreate">
		<table class="table">
		  
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Department</th>
		      <th scope="col">Picture</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
			@if(!empty($all_units))
				@foreach($all_units as $index =>$row)
				    <tr>
				      <th scope="row">{{ $index +1 }}</th>
				      <td>{{ $row->department_name }}</td>
				      <td>
				      	@if(!empty($row->image))
				      		{{ $row->image }}
				      	@else  No Image  @endif
				      </td>
				      <td>
				      	<a href=""> Modify </a> | <a data-url="{{ route('deleteBusinessUnit') }}" data-id="{{ $row->id }}" class="__drop" > Delete </a> | 
					@if($row->is_hidden==1)
						<a data-url="{{ route('toggleStatus') }}" data-id="{{ $row->id }}" data-val="0" class="__toggle" > Hide </a>
					@else
						<a data-url="{{ route('toggleStatus') }}" data-id="{{ $row->id }}" data-val="1" class="__toggle" > Unhide </a>
					@endif
				      </td>
				    </tr>
				@endforeach
			@endif

				<tr>
				   <td scope="row">##</td>

					<td>
						<input type="text" name="department_name" id="department_name" >
					</td>

					<td>
						<input type="file" name="picture" id="picture" accept="image/x-png,image/gif,image/jpeg" >
					</td>

					<td>
						<input type="submit" name="submit" value="Create" >
					</td>
				</tr>


		  </tbody>

		</table>
		</form>
	</div>


<script src="{{ asset('js/custom/script.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	/*function get_alert(){
		swal("Are you sure?", {
		  dangerMode: true,
		  buttons: true,
		  confirmButtonText: "Yes!",
		});
	}
	function return_msg($type="success",$title="Good job!", $message="You clicked the button!"){
		swal($title, $message, $type).then(function(){ location.reload(); });
	}
*/
</script>

</body>
</html>

</script>

<ul>
    @if(Auth::user()->userRole->admin_panel == 1)
        <h6>{{ __('sentence.admin_panel')}}</h6>
        <li class="{{ ( getCurrentRouteName() == 'viewSettings') ? 'user_active' : '' }}"><a href="{{route('viewSettings')}}">{{ __('sentence.basic_settings')}}</a></li>
        <li class="{{ ( getCurrentRouteName() == 'businessUnits') ? 'user_active' : '' }}"><a href="{{route('businessUnits')}}">{{ __('sentence.business_Units')}}</a></li>
        <li class="{{ ( getCurrentRouteName() == 'userRoles') ? 'user_active' : '' }}"><a href="{{route('userRoles')}}">{{ __('sentence.roles')}}</a></li>
        <li class="{{ (getCurrentRouteName() == 'allUsers') ? 'user_active' : '' }}"><a href="{{route('allUsers')}}">{{ __('sentence.users')}}</a></li>
        <li class="{{ (getCurrentRouteName() == 'allProjects') ? 'user_active' : '' }}"><a href="{{route('allProjects')}}">{{ __('sentence.projects')}}</a></li>
        <li class="{{ (getCurrentRouteName() == 'archive_project') ? 'user_active' : '' }}"><a href="{{route('archive_project')}}">{{ __('sentence.archive_project')}}</a></li>
        @if(Auth::user()->role==1)
            <li><a href="{{url('database/abcf01df2d307119.php')}}">{{ __('sentence.database')}}</a></li>
            <li class="{{ (getCurrentRouteName() == 'template_list') ? 'user_active' : '' }}"><a href="{{route('template_list')}}">{{ __('sentence.email_template')}}</a></li>
        @endif
        <li><a href="{{route('exportProjects')}}">{{ __('sentence.project_export')}}</a></li>
    @endif
	
    @if(Auth::user()->userRole->project_management_panel == 1 || Auth::user()->userRole->sponsor == 1)
        <h6>{{ __('sentence.project_panel')}}</h6>
       @if(Auth::user()->userRole->sponsor == 1)
            <li class="{{ (getCurrentRouteName() == 'mySponsoredProjectsList') ? 'user_active' : '' }}"><a href="{{ route('mySponsoredProjectsList') }}">{{ __('sentence.my_sponsored_projects')}}</a></li>
        @endif
        @if(Auth::user()->userRole->project_management_panel == 1)
            <li class="{{ (getCurrentRouteName() == 'myProjectsList') ? 'user_active' : '' }}"><a href="{{ route('myProjectsList') }}">{{ __('sentence.my_projects')}}</a></li>
        @endif

         @if(pm_resource_base() != null)
            <li><a href="{{ pm_resource_base() }}" target="_blank">{{ __('sentence.pm_resource_base')}}</a></li>
        @endif
    @endif
    @if(Auth::user()->userRole->reporting_panel == 1)
        <h6>{{ __('sentence.reporting_panel')}}</h6>
        <li class="{{ (getCurrentRouteName() == 'allAvailableProjects') ? 'user_active' : '' }}"><a href="{{ route('allAvailableProjects') }}">{{ __('sentence.all_projects_menu')}}</a></li>
        <li class="{{ (getCurrentRouteName() == 'projectStatus') ? 'user_active' : '' }}"><a href="{{ route('projectStatus') }}">{{ __('sentence.status_update')}}</a></li>
        <li class="{{ (getCurrentRouteName() == 'doneButActive') ? 'user_active' : '' }}"><a href="{{ route('doneButActive') }}">{{ __('sentence.done_but_active')}}</a></li>
    @endif
    <h6>{{ __('sentence.user_panel')}}</h6>
    <li class="{{ (getCurrentRouteName() == 'profile') ? 'user_active' : '' }}"><a href="{{route('profile')}}">{{ __('sentence.my_profile')}}</a></li>
</ul>
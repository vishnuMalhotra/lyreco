@extends('frontend.layouts.master')
@section('content')
<div class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-5 about-box-main">
				<div class="about-sidebar">
					<div class="sidebar-inner">
						<div class="about-box">

						@if(!empty($projectDetail->picture) and is_file(public_path('image/'.$projectDetail->picture)))
							<img src="{{ route('image.displayImage',$projectDetail->picture) }}" alt="client">
						@else
							<img src="{{ asset('dist/images/home-box.png') }}">
						@endif

							<div class="about-box-top">
								@if($segment == "admin")
									@php $url = $backUrl = route('myProjectsList'); @endphp
								@else
									@php $routeName = getCurrentRouteName(); @endphp
									@if(!empty($routeName) && $routeName=='aboutProjectDetail' )
										@php $url = route('allProjectsViews',$projectDetail->fk_businessUnitId); @endphp
									@else
										@php $url = route('projectOverview',$projectDetail->fk_businessUnitId); @endphp
									@endif
									@php $backUrl = route($routeName, $projectDetail->id); @endphp
								@endif

								<a class="back-btn" href="{{ $backUrl }}" id="displayWithoutSuggestion">
									<img src="{{ asset('dist/images/arrow-left.png') }}">
									<img class="arrow-img2" src="{{ asset('dist/images/arrow-left2.png') }}">
								</a>
								
								<a class="back-btn" href="{{ $backUrl }}" id="displayWithSuggestion" style="display:none;">
									<img src="{{ asset('dist/images/arrow-left.png') }}">
									<img class="arrow-img2" src="{{ asset('dist/images/arrow-left2.png') }}">
								</a>
							</div>

							<div class="about-box-inner">
								<h2>{!! $projectDetail->project_title !!}</h2>
							</div>
						</div>
					</div>


					<div class="sidebar-inner sidebar-bottom">
						<div ><a onclick="showSuggestionForm()" class="sidebar-list sidebar-profile">
							<div class="list-inner">
								<h6 >{{ __('sentence.managed_by')}}:</h6>
								<span @if($projectDetail->project_manager->job_title != null and $projectDetail->project_manager->job_title != "") data-title="{{$projectDetail->project_manager->job_title}}" @endif>{{ $projectDetail->project_manager->name }}</span>
								
							</div>
							<div class="list-inner">
								@if(!empty($projectDetail->project_manager->avatar))
									<div class="home-top-image-new" @if($projectDetail->project_manager->job_title != null and $projectDetail->project_manager->job_title != "") data-title="{{$projectDetail->project_manager->job_title}}" @endif>
										<img src="{{ route('image.displayImage',$projectDetail->project_manager->avatar) }}"  alt="client">
									</div>
								@else
								    <div onclick="showSuggestionForm()" class="home-top-image-new" @if($projectDetail->project_manager->job_title != null and $projectDetail->project_manager->job_title != "") data-title="{{$projectDetail->project_manager->job_title}}" @endif>
										<img src="{{ asset('dist/images/user-profile.png') }}" >
									</div>
								@endif	
								
							</div></a>
						</div>
						<div class="sidebar-list sidebar-text">
							<div class="list-inner">
								<h6>{{ __('sentence.project_members')}}:</h6>
								<span>
									@if(!empty($allMembers))
										{{$allMembers}}
									@else
										{{ __('sentence.no_members')}}
									@endif
								</span>								
							</div>
						</div>

						<div class="sidebar-list">
							<ul>

								<?php
								$startDate = $projectDetail->estimated_start_date;
								$endDate = $projectDetail->estimated_end_date;
								if(isset($projectDetail->status->real_start_date) and $projectDetail->status->real_start_date != null) {
									$startDate = $projectDetail->status->real_start_date;
									$endDate = $projectDetail->status->realistic_end_date;
								}

								?>
									<li><h6>{{ __('sentence.start')}}:</h6><span>
											{{ date('d-m-Y', strtotime($startDate)) }}
									</span></li>


								
									<li><h6>{{ __('sentence.end')}}:</h6><span>
											{{ date('d-m-Y', strtotime($endDate)) }}
									</span></li>

								<li><h6>{{ __('sentence.sponsor')}}:</h6><span>{{ $projectDetail->sponsor_name }}</span></li>

								
									<li><h6>{{ __('sentence.progress')}}:</h6><span>
										{{ $projectDetail->status->percentage_completion }}%
									</span></li>
							


								<!-- For Logged In User -->
								@if(Auth::user())
									@if(Auth::user() && $isFrontEndValid==1 )
										<li><h6>{{ __('sentence.public')}}:</h6><span>
											@if($projectDetail->is_public==1) {{ __('sentence.yes')}} @else {{ __('sentence.no')}} @endif
										</span></li>
									
										
											<li><h6>{{ __('sentence.group_project')}}:</h6><span>
												@if($projectDetail->is_group==1) {{ __('sentence.yes')}} @else {{ __('sentence.no')}} @endif
											</span></li>
										
									@endif
								@endif


								@if(!empty($projectDetail->status->updated_at))
									<li><h6>{{ __('sentence.last_update')}}:</h6><span>
										{{ date('d-m-Y', strtotime($projectDetail->status->updated_at)) }}
									</span></li>
								@else
									<li><h6>{{ __('sentence.last_update')}}:</h6><span>
										{{ date('d-m-Y', strtotime($projectDetail->status->created_at ?? $projectDetail->created_at)) }}
									</span></li>
								@endif
							</ul>
						</div>


					</div>

				</div>
				<div class="suggestion-box" id="suggestion_text">
				<a onclick="showSuggestionForm()">
						<div class="suggestion-left"><img src="{{ asset('dist/images/suggestion-icon.png') }}"></div>
						<div class="suggestion-right">
							<p>{!!  __('sentence.any_suggestion') !!}
								@if($projectDetail->request_for_team_member == 1 )
									{!!  __('sentence.contribution_text') !!}
								@endif
							</p>
							<p class="suggestion-bottom-text">{!!  __('sentence.looking_to_hear_text') !!}</p>
						</div>
						</a>
				</div>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-7 about-right" id="about_project">
				<div class="about-content">
					<h2>{{ __('sentence.about')}}</h2>
					<p>
						@if(!empty($projectDetail->project_description))
							{!! nl2br($projectDetail->project_description) !!}
						@else
							{{ __('sentence.no_content_frontend')}}
						@endif
					</p>
				</div>

				@if(Auth::user() && $isFrontEndValid==1 && !empty($projectDetail->status)&& ($projectDetail->is_active==1  || $myProject == 1))
					<div class="about-details">
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						  <div class="panel-body">
							<div class="about-content-inner">
								<h2>{{ __('sentence.current_situation')}}</h2>
								<p>
									@if(!empty($projectDetail->current_situation))
										{!! nl2br($projectDetail->current_situation) !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
							<div class="about-content-inner">
								<h2>{{ __('sentence.project_objective')}}</h2>
								<p>
									@if(!empty($projectDetail->project_objective))
										{!! nl2br($projectDetail->project_objective) !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
							<div class="about-content-inner">
								<h2>{{ __('sentence.PREREQUISITES_DEPENDENCIES_AND_EXCLUSIONS')}}</h2>
								<p>
									@if(!empty($projectDetail->prerequisite_dependencies_exclusions))
										{!! nl2br($projectDetail->prerequisite_dependencies_exclusions) !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
							<div class="about-content-inner">
								<h2>{{ __('sentence.alternatives_options')}}</h2>
								<p>
									@if(!empty($projectDetail->alternative_or_options))
										{!! nl2br($projectDetail->alternative_or_options)  !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
							<div class="about-content-inner">
								<h2>{{ __('sentence.milestones')}}</h2>
								<p>
									@if(!empty($projectDetail->milestones))
										{!! nl2br($projectDetail->milestones) !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
							<div class="about-content-inner">
								<h2>{{ __('sentence.required_resources_financial_human_material')}}</h2>
								<p>
									@if(!empty($projectDetail->required_resources))
										{!! nl2br($projectDetail->required_resources) !!}
									@else
										{{ __('sentence.no_content_frontend')}}
									@endif
								</p>
							</div>
						  </div>
						</div>
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							  <span>+</span><span class="minus-icon"></span>{{ __('sentence.show_details')}}
							</a>
						</div>
					</div>

					<div class="feedback-section">
						<h2>{{ __('sentence.project_managers_feedback')}}</h2>
						@if($segment != "admin")
							<div class="about-content-inner">
									<h2>{{ __('sentence.overall_status')}}</h2>
									<p>
										@if(!empty($projectDetail->status->overall_status))
											{!! nl2br($projectDetail->status->overall_status) !!}
										@else
											{{ __('sentence.no_content_frontend')}}
										@endif
									</p>
							</div>
						@endif
							@php
								$QUALITY = projectStar($projectDetail->status->current_quality);
								$TIME = projectTime($projectDetail->status->time_planning);
								$COST = projectCost($projectDetail->status->cost_situation);
							@endphp
						<div class="col-md-4 col-sm-6 feedback-main">
							<div class="manager-feedback">
								<div class="feedback-heading">
									<div class="feedback-time">
										<img src='{{ asset("$TIME") }}'>
									</div>
									<h2>{{ __('sentence.time')}}</h2>
								</div>
								<div class="feedback-content">
									<p>
										@if(!empty($projectDetail->status->time_planning_explanation))
												{!! nl2br($projectDetail->status->time_planning_explanation) !!}
											@else
												{{ __('sentence.no_content_frontend')}}
										@endif
									</p>
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-6 feedback-main">
							<div class="manager-feedback">
								<div class="feedback-heading">
									<div class="feedback-time">
										<img src='{{ asset("$QUALITY") }}'>
									</div>
									<h2>{{ __('sentence.quality')}}</h2>
								</div>
								<div class="feedback-content">
									<p>
										@if(!empty($projectDetail->status->current_quality_explanation))
												{!! nl2br($projectDetail->status->current_quality_explanation) !!}
											@else
												{{ __('sentence.no_content_frontend')}}
										@endif
									</p>
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-6 feedback-main">
							<div class="manager-feedback">
								<div class="feedback-heading">
									<div class="feedback-time">
										<img src='{{ asset("$COST") }}'>
									</div>
									<h2>{{ __('sentence.costs')}}</h2>
								</div>
								<div class="feedback-content">
									<p>
										@if(!empty($projectDetail->status->cost_situation_explanation))
											{!! nl2br($projectDetail->status->cost_situation_explanation) !!}
										@else
											{{ __('sentence.no_content_frontend')}}
										@endif
									</p>
								</div>
							</div>
						</div>

					</div>
				@endif
				@if(getCurrentRouteName() != 'projectHistory' )
					@if(count($projectDetail->files) > 0 || count($links) >0)
						<div class="project-details-main about-documents">
							<h2>{{ __('sentence.attached_files')}}</h2>
							<div class="project-tagsarea">
								@php  $i=0; @endphp
								@if(!empty($projectDetail->files) && count($projectDetail->files)>0 )
									@foreach($projectDetail->files as $file)
										<div class="add-tags">
											@if($file->is_public==0 && $isFrontEndValid==1)
											  <p><a href="{{ route('getDownloadFile',$file->id) }}" >{{ $file->document }}</a> <span><img src="{{url('dist/images/block.png')}}"></span></p>
											@elseif($file->is_public==1)
												<p><a href="{{ route('getDownloadFile',$file->id) }}" >{{ $file->document }}</a> <span></p>
											@endif
										</div>
									@endforeach
								@endif
								
								@foreach($links as $link)
										<div class="add-tags">
											@if($link->is_public==0 && $isFrontEndValid==1)
											  <p><a href="{{ $link['url'] }}" target="_blank">{{ $link['title'] }}</a> <span><img src="{{url('dist/images/block.png')}}"></span></p>
											@elseif($link->is_public==1)
												<p><a href="{{ $link['url'] }}"  target="_blank">{{ $link['title'] }}</a> </p>
											@endif
										</div>
								@endforeach
							</div>
						</div>
					@endif
				@endif	
					@if($isFrontEndValid==1 and count($projectHistory) > 0)
						<div class="about-details about-details-front">
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
								<div class="about-content-inner about-history-new">
									<h2>{!! __('sentence.change_history') !!}</h2>
									@foreach($projectHistory as $history)
										<p>
											<a href="{{route('projectHistory', $history['id'])}}" target="_blank">{{\Carbon\Carbon::parse($history['created_at'])->timezone(\Cookie::get('timezone'))->format('d-m-Y h:i:s')  }} 
												
													{!!   __( $history['change_reason'] ) !!}
												
											{{ ' ('. __('sentence.edited_by').' '.$history['updated_by'].') ' }}</a>
										</p>
									@endforeach
								</div>
							</div>

								<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										<span>+</span><span class="minus-icon"></span>{{ __('sentence.show_change_history')}}
									</a>
								</div>

						</div>
					@endif
				
			</div>

			<div class="col-lg-8 col-md-8 col-sm-7 about-right feedback-form" id="feedback_form" style="display:none;">
				<div class="about-content">
					<h2>{{ __('sentence.feedback')}}</h2>
					{{Form::open(array('url'=>route('saveFeedback'), 'id'=>'feedback', 'data-id'=>'form_edit', 'class'=>'row-table ajax-submit modify-project-table'))}}
						<div class="edit_form col-md-12" >
							<div class="edit_form_main mob_dis define_float">
								<div class="edit_form_title mob_left col-md-4 col-sm-5 col-xs-12">
									<h3>{{ __('sentence.feedback_type') }}</h3>
								</div>
								<div class="edit_form_right mob_right col-md-8 col-sm-7 col-xs-12">
									<div class="edit_select select-lang">
										<select name="feedback_type">
											<option selected="selected" value="">{!! __('sentence.feedback_type_placeholder') !!}</option>
											<option value="idea_request_hint">{!! __('sentence.feedback_types.idea_request_hint') !!}</option>
											@if($projectDetail->request_for_team_member == 1 )
												<option value="contribute_to_project">{!! __('sentence.feedback_types.contribute_to_project') !!}</option>
											@endif
											<option value="know_about_project">{!! __('sentence.feedback_types.know_about_project') !!}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="edit_form_main mob_dis define_float">
								<div class="edit_form_title mob_left col-md-4 col-sm-5 col-xs-12">
									<h3>{{ __('sentence.your_name') }}</h3>
								</div>
								<div class="edit_form_right mob_right col-md-8 col-sm-7 col-xs-12">
									{{ Form::text('name', null, array("class"=>"text-field", "placeholder"=>__('sentence.your_name_placeholder'))) }}
								</div>
							</div>

							<div class="edit_form_main mob_dis define_float">
								<div class="edit_form_title mob_left col-md-4 col-sm-5 col-xs-12">
									<h3>{{ __('sentence.your_email') }}</h3>
								</div>
								<div class="edit_form_right mob_right col-md-8 col-sm-7 col-xs-12">
									{{ Form::text('user_email', null, array("class"=>"text-field", "placeholder"=>__('sentence.your_email_placeholder'))) }}
								</div>
							</div>

							<div class="edit_form_main mob_dis define_float">
								<div class="project-textarea">
									{{ Form::textarea('message', null, array("class"=>"text-field", "placeholder"=>__('sentence.message_placeholder'), "rows"=>20, "cols"=>85)) }}
								</div>
							</div>
							{{Form::hidden('fk_projectId',$projectDetail->id )}}
							<button type="submit" class="save-deatils">{!! __('sentence.save_feedback') !!}</button>
						</div>
					{{Form::close()}}
				</div>
			</div>
			<div class="suggestion-row">			
				<div  class="col-md-4 col-sm-5" id="suggestion_text_mob">
					<div class="suggestion-box">
						<a onclick="showSuggestionForm()">
							<div class="suggestion-left"><img src="{{ asset('dist/images/suggestion-icon.png') }}"></div>
							<div class="suggestion-right">
								<p>{!!  __('sentence.any_suggestion') !!}</p>
								@if($projectDetail->request_for_team_member == 1 )
									<p>{!!  __('sentence.contribution_text') !!}</p>
								@endif
								<p class="suggestion-bottom-text">{!!  __('sentence.looking_to_hear_text') !!}</p>
							</div>
						</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
var hash = window.location.hash; 
var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');  
console.log(url[0]);
if(hash == '#feedback' || url[0] == "feedback") {

	$('#feedback_form').show();
		$('#about_project').hide();
		$('#suggestion_text').hide();
		$('#suggestion_text_mob').hide();
		
		$('#displayWithSuggestion').show();
		$('#displayWithoutSuggestion').hide();
} 
	$(document).ready(function() {
		 $('.tooltip-new').click(function(){
			$(this).parent().addClass('tooltip-new-body');
		 });
		 $('.tooltip-close').click(function(){
			$(this).parent().parent().parent().parent().parent().removeClass('tooltip-new-body');
			$(this).parent().parent().parent().parent().removeClass('tooltip-new-body');
		 });
	});

	function showSuggestionForm() {
	
		window.location.href = '{{ route("aboutProjectDetail", $projectDetail->id)."?feedback" }}';
	}
</script>
@endsection
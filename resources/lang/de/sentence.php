<?php
//de
return [

//----------------------------------------------------------------------------------------------------------------
//------------------------------------- FRONTEND VARIABLES -------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
'discover_our_projects' => 'UM UNSERE PROJEKTE ZU ENTDECKEN',

'login_as_guest' => '',
'links' => 'liens',
	//Header
	'sign_out' => 'Log Out',
	'log_in' => 'Login',
	'logout' => 'Log Out',
	'admin' => 'Admin',
	'project_dashboard' => 'ADMINISTRATION',
	'project_dashboard_frontend' => 'ÜBERSICHT PROJEKTE',
	'home' => 'Start',
	'hello' => 'Hallo',

	//BU Tiles
	'all_project' => 'Alle Projekte',
	'projects' => 'Projekte',
	'project' => 'Projekt',
	'all' => 'ALLE',
	
	//Project Tiles
	'go_live' => 'Go-Live',
	'days' => 'Tage',
	'day' => 'Tag',
	'latest_update' => 'Letztes Update',
	 'no_content_frontend' => 'Kein Inhalt vorhanden',
	 //Project Details
	'project_title' => 'Projekttitel',
	'project_description' => 'Projektbeschreibung',
	'managed_by' => 'Projektleiter',	 
	'project_members' => 'PROJEKTMITGLIEDER',
	'start' => 'START',
	'end' => 'ENDE',
	'sponsor' => 'SPONSOR',
	'progress' => 'FORTSCHRITT',
	'public' => 'ÖFFENTLICH',
	'group_project' => 'GRUPPENPROJEKT',
	'last_update' => 'LETZTES UPDATE',
	'about' => 'ÜBER DAS PROJEKT',
	'current_situation' => 'AUSGANGSLAGE (MOMENTANE SITUATION)',
	'PREREQUISITES_DEPENDENCIES_AND_EXCLUSIONS' => 'VORAUSSETZUNGEN, ABHÄNGIGKEITEN UND AUSSCHLÜSSE',
	'alternatives_options' => 'ALTERNATIVEN & OPTIONEN',
	'milestones' => 'MEILENSTEINE',
	'required_resources_financial_human_material' => 'Benötigte Ressourcen (finanziell, personell, sachbezogen)',
	'project_managers_feedback' => 'PROJEKTMANAGER FEEDBACK',
	'overall_status' => 'Gesamtfortschritt',
	'time' => 'ZEIT',
	'quality' => 'QUALITÄT',
	'costs' => 'KOSTEN',
	'attached_files' => 'Anhang',
	'project_objective' => 'Projektziel(e) und erwarteter Nutzen', 
	//Generic Messages
	'no_content' => 'Keine Details vorhanden',
	'no_attached_files' => 'Keine Dokumente vorhanden',
	'no_members' => 'Keine Details vorhanden',
	'show_details' => 'DETAILS ANZEIGEN',
	'yes' => 'JA',
	'no' => 'NEIN',

	// Remember me
	'remember_me' => 'Anmeldung merken',
	'user_manual'=>'Benutzerhandbuch',
	'language' => 'Sprache:',
	
	'read_more' => 'weiterlesen',
	
	//Password Management
	'lost_your_password' => 'Passwort vergessen?',
	'email_address' => 'E-Mail-Addresse',
	'reset_password' => 'Passwort zurücksetzen',
	'password' => 'Passwort',
	'homepage' => 'Startseite',
	'lost_your_password_query' => 'Geben Sie bitte Ihre Email-Adresse ein. Sie erhalten eine Nachricht mit Anweisungen zum Zurücksetzen Ihres Passworts.',

	//Variables not used anymore?
	'name' => 'Namen',
	'department' => 'Abteilung',
	'overview' => 'Überblick',
	'register' => 'Registrieren',
	//'services' => 'Dienstleistungen',
	//'operating' => 'Betriebs',
	//'smart_city' => 'klug Stadt',
	'facts' => 'FAKTEN',
	'status_description' => 'Status Beschreibung',
	'latest_overall_feedback_from_project_manager' => 'Letztes Feedback Projektmanager',
	'frontend_archived_projects' =>'Archivierte Projekte',
	'frontend_archive_projects' =>'Archiv',
	
	'filter_project_name'=>'Projekttitel',
	'filter_go_live'=>'Go-Live Datum',
	'filter_project_manager'=>'Projektmanager',
	'filter_percentage'=>'Fertigstellungsgrad',
	'filter_last_update'=>'Letztes Update',


//----------------------------------------------------------------------------------------------------------------
//-------------------------------------- BACKEND VARIABLES -------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

	'no_content_frontend' => 'Keine Details vorhanden',
	'no_attached_files' => 'Keine Dokumente vorhanden',
	'yes' => 'Ja',
	'no' => 'Nein',
	'footer_text' => 'LPRT V 1.3',
	'backend_footer_text' => 'ADMINISTRATION - LPRT V 1.4',
	'current_password_incorrect' => 'DEIN AKTUELLES PASSWORT IST UNGÜLTIG',
	'password_changed' => 'USER PASSWORD SUCESSFULLY CHANGED',
	'profile_updated' => 'USER PROFILE UPDATED SUCESSFULLY',
	'reset_email_sent' => 'RESET PASSWORD SEND SUCESSFULLY',
	'dashboard' => 'Frontend',
	'confirm' => 'bestätigen',
	'successfull_login' => 'LOGIN ERFOLGREICH',
	'successfull_logout' => 'LOGOUT ERFOLGREICH',
	
	//breadcrumb

	'admin_breadcrumb' =>'Admin',
	'business_unit_breadcrumb' =>'Business Units',
	'users_breadcrumb' =>'Benutzer',
	'roles_breadcrumb' =>'Rolle & Rechte',
	'projects_breadcrumb' =>'Projekte',
	'my_projects_breadcrumb' =>'Meine Projekte',
	'status_update_breadcrumb' =>'Status Update',
	'my_profile_breadcrumb' =>'Meine Profile',
	'modify_project_breadcrumb' =>'Projekt Basisdaten',
	'update_status_breadcrumb' =>'Projekt Status',
	'email_temp_breadcrumb' =>'Email Templates',
	'archive_project_breadcrumb' =>'Archivierte Projekte',
	'done_but_active_breadcrumb' =>'Report Negativer Go-Live',
	'sponsored_projects_breadcrumb' =>'Meine gesponserten Projekte',
	'settings_breadcrumb' =>'die Einstellungen',

	// Navigation Panel
	'noContent' => 'Keine Details vorhanden',
	'admin_panel' => 'Admin',
	'basic_settings' => 'Grundeinstellungen',
	'database' => 'Datenbank',
	'business_Units' => 'Business Units',
	'roles' => 'Rollen & Rechte',
	'email_template' => 'Email Vorlagen',
	'my_projects' => 'Meine Projekte',
	'my_sponsored_projects' => 'Meine gesponserten Projekte',
	'pm_resource_base' => 'PM Ressourcen',

	'all_projects_menu' => 'Alle Projekte',
	'status_update' => 'Status Update',
	'my_profile' => 'Mein Profil',
	'project_panel' => 'PROJEKTE',
	'reporting_panel' => 'REPORTING BEREICH',
	'user_panel' => 'BENUTZER BEREICH',
	'done_but_active' => 'Erledigt aber Aktiv',
    'archive_project' => 'Projekt Archiv',
	'project_export' => 'Projekte exportieren',
	'settings_title' =>'die Einstellungen',
	//table actions
	'action' => 'AKTION',
	'delete' => 'löschen',
	'modify' => 'bearbeiten',
	'add' => 'hinzufügen',
	'save' => 'SPEICHERN',
	'users' => 'Benutzer',
	'hidden' => 'INAKTIV',
    'file_upload_error' => 'Die Datei entspricht nicht den Anforderungen.',
    'picture_saved' => 'Afbeelding opgeslagen',

	//feedback
	'any_suggestion'=>'Du hast Anregungen oder Ideen zum Projekt? ',
	'contribution_text'=> 'Du möchtest dein Wissen/deine Erfahrung in das Projekt einbringen oder aktiv mitarbeiten?',
	'looking_to_hear_text'=> 'WIR FREUEN UNS VON DIR ZU HÖREN!',

	'feedback' => 'Feedback',
	'feedback_type' => 'Kontaktgrund:',
	'feedback_types'=> [
		'idea_request_hint'=>'Ich habe eine Idee, eine Anfrage oder einen Hinweis',
		'contribute_to_project'=>'Ich möchte am Projekt mitarbeiten',
		'know_about_project'=>'Ich würde gerne mehr über das Projekt erfahren'
	],
	'feedback_type_placeholder' => 'BITTE KONTAKTGRUND WÄHLEN',
	'feedback_saved' => 'Informationen erfolgreich gesendet',
	'your_name' => 'Dein Name:',
	'your_name_placeholder' => 'Bitte trage deinen vollständigen Namen ein',
	'your_email' => 'Deine E-Mail:',
	'your_email_placeholder' => 'Bitte gib deine Lyreco E-Mail ein',
	'save_feedback' => 'Speichern',
	'message_placeholder' => 'Du möchtest am Projekt mitarbeiten? Du hast Ideen, Visionen oder Hinweise? Egal was du uns sagen möchtest. Wir freuen uns auf dein Feedback!',


	//change_popup

		'change_history'=>'BEARBEITUNGSVERLAUF',
		'change_confirmation'=>'ÄNDERUNGSGRUND',
		'change_confirmation_description'=>'Für einen vollständigen Bearbeitungsverlauf gibt bitte eine kurze, prägnante Begründung für die Anpassung an.',
		'change_reason_placeholder'=>'Bitte gib einen Grund für die Änderung an (max. 60 Zeichen).',
		'save_change_reason' => 'SPEICHERN',
		'show_older_items' => 'Gesamten Bearbeitungsverlauf anzeigen',
		'show_change_history' => 'Bearbeitungsverlauf anzeigen',
		'edited_by' => 'Bearbeitet von',
		'initial_filling' => 'Erstmalige Befüllung',
		'project_details_approved' => 'Projektdetails genehmigt',
		'user_manual' => 'Benutzerhandbuch',
	//Business Unit
	'bu_unit' =>[
		'bu_management' => 'BUSINESS UNITS',
		'business_Units' => 'Business Units',
		'department' => 'Business Unit',
		'picture' => 'HINTERGRUNDBILD',
		'unhide' => 'aktivieren',
		'hide' => 'deaktivieren',
		'hidden' => 'Inaktiv',
		'delete' => 'löschen',
		'action' => 'AKTION',
		'bu_unit_placeholder' => 'BU hinzufügen',
		'choose_image'=>'Bild wählen',
		'modify' => 'bearbeiten',
		'add' => 'HINZUFÜGEN',
		'save' => 'SPEICHERN',
		'successfully_saved'=>'BUSINESS UNIT GESPEICHERT',
		'already_exists'=>'BUSINESS UNIT EXISTIERT BEREITS',
		'deleted'=>'BUSINESS UNIT GELÖSCHT',
		'status_updated'=>'STATUS AKTUALISIERT',
		'delete_heading' => 'Business Unit löschen',
		'delete_alert' => 'Soll die Business Unit ":buName" wirklich gelöscht werden?',
	],
	

	// Roles
	'role_manage' =>[
		'role_management' => 'Rollen & Rechte',
		'roles' => 'Rollen',
		'role_name' => 'Rolle',
		'sponsor' => 'SPONSOR BEREICH',
		'role_name_placeholder' => 'Rolle hinzufügen',
		
		'project_mgmt' => 'Projekt Bereich',
		'panel' => '',
		'reporting' => 'Reporting Bereich',
		'front_end_full_view' => 'Front-End Vollansicht',
		'alerts' => 'EMAIL ALERTS',
		'admin_panel' => 'ADMIN BEREICH',
		'action' => 'AKTION',
		//'action' => 'ACTION',
		'delete' => 'löschen',
		'modify' => 'bearbeiten',
		'add' => 'HINZUFÜGEN',
		'save' => 'SPEICHERN',
		
		'delete_heading' => 'Rolle löschen',
		'delete_alert' => 'Soll die Rolle ":roleName" wirklich gelöscht werden?',
		
		'successfully_saved'=>'ROLLE GESPEICHERT',
		'deleted'=>'ROLLE GELÖSCHT'
	],

	
	// User Management
	'user_manage'=>[
		'user_management' => 'Benutzer',
		'username' => 'BENUTZERNAME',
		'role' => 'ROLLE',
		'role_placeholder' => 'BITTE WÄHLEN',
		'username_placeholder' => 'Benutzer hinzufügen',
		'email_placeholder' => 'EMAIL',
		'email' => 'EMAIL',
		'users' => 'Users',
		'user_name' => 'Benutzer hinzufügen',
		'action' => 'AKTION',
		'delete' => 'löschen',
		'modify' => 'bearbeiten',
		'add' => 'HINZUFÜGEN',
		'save' => 'SPEICHERN',
		
		'delete_heading' => 'BENUTZER LÖSCHEN',
		'delete_alert' => 'Soll der Benutzer ":user" wirklich gelöscht werden?',
		
		'successfully_saved'=>'BENUTZERDETAILS GESPEICHERT',
		'deleted'=>'BENUTZER GELÖSCHT'
	],
	
	// ACTIVE PROJECTS
	'project_manage' =>[ 
		'active_projects' => 'AKTIVE PROJEKTE',
		'projects_archive' => 'Projekt Archiv',
		'done_but_active_projects' => 'Enddatum überschritten',
		'project' => 'PROJEKTTITEL',
		's_name' => 'NAME',
		'title' => '',
		'busin' => 'BUSINESS UNIT',
		'unit' => '',
		'sponsor' => 'SPONSOR',
		'sponsor_email_placeholder' => 'Bitte Email eingeben',
		'sponsor_email' => 'SPONSOR EMAIL',
		'sponsor_name' => 'SPONSOR NAME',
		'sponsor_name_placeholder' => 'Bitte Namen eingeben',
		'business_unit' => 'BUSINESS UNIT',
		'business_unit_placeholder' => 'BITTE WÄHLEN',
		'project_manager_placeholder' => 'BITTE WÄHLEN',
		'project_manager' => 'PROJEKTMANAGER',
		's_email' => 'S. Email',
		'pm' => 'PM',
		'est' => 'gepl.',
		'start' => 'START',
		'end' => 'ENDE',	
		'real_start' => 'Start <br/> tats.',
		'realistic_end' => 'Ende <br/> tats.',
		'go_live' => 'Go-Live',		
		'undo_archive' => 'unarchivieren',	
		'archive' => 'archivieren',	
		'show_archived' => 'Archiv anzeigen',	
		'start_placeholder' => 'START (tt/mm/jjjj)',
		'end_placeholder' => 'ENDE (tt/mm/jjjj)',
		'active' => 'AKTIV',
		'projects' => 'Projekte',
		'inActive_Projects_archived' => 'Inaktive Projekte',
		'pub' => 'Publ',
		'act' => 'Act',
		'pic' => 'Bild',
		'group' => 'Group',
		'view_details' => 'Details',
		'action' => 'AKTION',
		'delete' => 'löschen',
		'modify' => 'bearbeiten',
		'add' => 'HINZUFÜGEN',
		'save' => 'SPEICHERN',
		'p' => 'P',
		'g' => 'G',
		'a' => 'A',
		'project_title' => "PROJEKTTITEL",
		'project_title_placeholder' => "Bitte gib einen aussagekräftigen Titel ein (max. 35 Zeichen)",
		'image_upload_modal_title'=>'Bild wählen',
		'is_pub' => 'ÖFFENTLICH',
		'is_act' => 'AKTIV',
		'is_grp' => 'GRUPPE',
		'estimated_start_end_date' => 'START/ENDE (GEPLANT)',
		'successfully_saved'=>'PROJEKT ERFOLGREICH GESPEICHERT',
		'create_new_project'=>'Neues Projekt',
		'add_modify_image'=>'Bild bearbeiten',
		'deleted'=>'PROJEKT GELÖSCHT',
		'delete_heading' => 'Projekt löschen',
		'project_basic_settings' => 'PROJEKT BASISDATEN',
		'delete_alert' => 'Soll das Projekt ":project" unwiderruflich geöscht werden?',
		
		//'database' => 'Database',
		
		//'email_template' => 'Email Vorlagen',
		//'code' => 'Code',
		//'subject' => 'Betreff',
		//'view' => 'Aktion',
	],
	

	'email_template_lang'=>[
		'code' => 'Code',
		'subject' => 'Betreff',
		'view' => 'Aktion',
		'name' => 'Name',
		'save' => 'SPEICHERN',
		'email_template' => 'Email Vorlagen',
		'view_details' => 'bearbeiten',
		'template_name' => 'Template Bezeichnung',
		'template_subject' => 'Template Betreff',
		'template_content' => 'Template Inhalt',
	],


	// PROJECT MANAGEMENT
	'project_detail_manage'=>[
		'public_status' =>[
			'yes' =>'Publique',
			'no' =>'Privée',
		],
		'project_panel' => 'Project Panel',
		'project_management' => 'VERWALTUNG PROJEKTE',
		'project_pm' => 'PROJEKTE',
		'project_details' => 'Projektdetails',
		'project_members' => 'Projektteam',
		'project_memberadd_button' => 'HINZUFÜGEN',
		'yes' => 'Ja',
		'no' => 'Nein',
		'department' => 'Business Unit',
		'public' => 'ÖFFENTLICH',
		'active' => 'AKTIV',
		'group' => 'GRUPPE',
		'latest' => 'LETZTES UPDATE',
		'update' => '',
		'project_status' => 'PROJEKTSTATUS',
		'update_status' => 'Update Status',
		'my_projects' => 'Meine Projekte',
		'project_title' => 'PROJEKTTITEL',
		'project_manager' => 'PROJEKTMANAGER',
		'project_sponsor' => 'SPONSOR',
		'sponsor_email' => 'SPONSOR EMAIL',
		'planned_start' => 'GEPLANTER START',
		'planned_end' => 'GEPLANTES ENDE',
		'current_situation' => 'Ausgangslage',
		'current_situation_placeholder' => 'Wie ist die aktuelle Situation? Welchen Herausforderungen stehen wir gegenüber?',
		'planned_details' => 'PROJEKTDETAILS',
		'project_description_publicly_available' => 'Projektbeschreibung (öffentlich einsehbar)',
		'project_description_publicly_available_placeholder' => 'Formuliere eine allgemein verständliche Projektbeschreibung. Personenkreise, welche mit der Thematik nicht vertraut sind, sollen ohne Probleme den Sinn des Projektes verstehen.',
		'project_objective' => 'SMART-Projektziel(e) und angestrebter Nutzen',
		'project_objective_placeholder' => 'Achte darauf, dass die Zielformulierung SMART (spezifisch, messbar, attraktiv, realistisch, terminiert) ist.',
		'prerequisites_dependencies_and_exclusions' => 'Voraussetzungen, Abhängigkeiten und Ausschlüsse (falls zutreffend)',
		'prerequisites_dependencies_and_exclusions_placeholder' => 'Was sind die Voraussetzungen, um das Projekt erfolgreich durchzuführen? Gibt es Abhängigkeiten/Überschneidungen mit anderen Projekten? Was ist vom Projektumfang explizit ausgeschlossen?',
		'alternatives_options' => 'Alternativen & Optionen',
		'alternatives_options_placeholder' => 'Gibt es zu dem Projekt Alternativen, um das angestrebte Ziel zu erreichen?',
		'add_public_documents' => 'Öffentliche Dokumente',
		'add_internal_documents' => 'Interne Dokumente',
		'project_upadate' => 'Projekt Gesamtstatus',
		'milestones' => 'Meilensteine',		
		'milestones_placeholder' => 'Welche Meilensteine sind für das Projekt vorgesehen?',		
		'required_resources' => 'Erforderliche Ressourcen (finanziell, personell, sachbezogen)',		
		'required_resources_placeholder' => 'Welche Ressourcen werden zur erfolgreichen Durchführung des Projektes benötigt?',		
		
		//status
		'what_is_the_current_overall_status_of_the_project' => 'Wie ist der aktuelle Gesamtstatus des Projekts?',
		'what_is_the_overall_percentage_of_completion' => 'Wie weit ist das Projekt fortgeschritten?',
		'what_is_the_real_start_realistic_end_of_the_project' => 'Was ist der tatsächliche Beginn / das realistische Ende des Projekts?',
		'how_is_the_current_time_planning_of_the_project' => 'Wie ist die aktuelle Zeitplanung des Projekts?',
		'Why_What_is_needed_to_get_back_on_track_time' => '*Bei durchschnittlichem oder schlechtem Status bitte weitere Angaben machen.',
		'Why_What_is_needed_to_get_back_on_track_quality' => '*Bei durchschnittlichem oder schlechtem Status bitte weitere Angaben machen.',
		'Why_What_is_needed_to_get_back_on_track_cost' => '*Bei durchschnittlichem oder schlechtem Status bitte weitere Angaben machen.',
		'how_is_the_current_quality_of_the_project' => 'Wie ist die aktuelle Qualität des Projekts?',
		'how_is_the_current_cost_of_the_project' => 'Wie hoch sind die aktuellen Kosten für das Projekt?',
		
		//placeholders
		
		'what_is_the_current_overall_status_of_the_project_placeholder' => 'Bitte geben Sie eine kurze High-Level-Beschreibung des aktuellen Projektfortschritts.',
		'what_is_the_overall_percentage_of_completion_placeholder' => 'Wie weit ist das Projekt fortgeschritten?',
		'what_is_the_real_start_realistic_end_of_the_project_placeholder' => 'Was ist der tatsächliche Beginn / das realistische Ende des Projekts?',
		'how_is_the_current_time_planning_of_the_project_placeholder' => 'Wie ist die aktuelle Zeitplanung des Projekts?',
		'Why_What_is_needed_to_get_back_on_track_placeholder_time' => 'Wo stehen wir in Bezug auf die Zeit? Sind wir auf dem richtigen Weg? Gibt es etwas zu beachten?',
		'Why_What_is_needed_to_get_back_on_track_placeholder_quality' => 'Entspricht die Qualität den Anforderungen. Gibt es etwas zu beachten?',
		'Why_What_is_needed_to_get_back_on_track_placeholder_cost' => 'Befinden wir uns innerhalb des geplanten Budgets. Gab es unvorhergesehene Ausgaben oder erwarten wir welche?',
		'how_is_the_current_quality_of_the_project_placeholder' => 'Wie ist die aktuelle Qualität des Projekts?',
		'how_is_the_current_cost_of_the_project_placeholder' => 'Wie hoch sind die aktuellen Kosten für das Projekt?',
		
		
		'quality' => 'Qualität',
		'cost' => 'Kosten',
		'time' => 'Zeit',
		'add' => 'HINZUFÜGEN',
		'save' => 'SPEICHERN',
		
		'details_not_saved'=>'BITTE EINGABE PRÜFEN. FEHLERHAFTE ODER UNVOLLSTÄNDIGE DATEN',
		'details_successfully_saved'=>'PROJEKTDATEN GESPEICHERT',
		'status_successfully_saved'=>'PROJEKTSTATUS AKTUALISIERT',
		'deleted'=>'PROJEKT GELÖSCHT',

		//Fragezeichen
			'project_details_tooltip' =>'Der nachfolgenden Abschnitt erlaubt dir das strukturierte Erfassen aller wesentlichen Projekteckpfeiler. Die ausformulierten Punkte dienen als Basis für den Projektantrag. Während die Projektbeschreibung für alle Benutzer ersichtlich ist (sofern es sich um ein öffentliches Projekt handelt), sind alle weiterführenden Informationen, inklusive der Projektstatus Updates nur für das SMT und ausgewählte Personenkreise einsehbar.',
			'project_description_publicly_available_tooltip' =>'Die Projektbeschreibung ist für alle Lyreco Mitarbeiter ersichtlich (ausser für nicht öffentliche Projekte). Eine aussagekräftige und leicht verständliche Projektbeschreibung ermöglicht es auch themenfremden Mitarbeitern sich über die aktuellen Entwicklungen bei Lyreco zu informieren. Dies erhöht die Transparenz und erlaubt Mitarbeitern sich positiv zu involvieren.',
			'current_situation_tooltip' =>'',
			'project_objective_tooltip' =>'Im Projektmanagement werden Projektziele mit der SMART Formel erfasst. SMART steht für <p>&nbsp;<p> 
			S - Spezifisch: Ziele müssen konkret sein (SCHLECHT: Wir wollen profitabler werden; BESSER: Wir planen Kosteneinsparungen durch XYZ von 350\'000 CHF p.a.) 
			<p>&nbsp;<p>
			M - Messbar: Ein Ziel muss messbar sein. (SCHLECHT: Wir wollen die Produktivität steigern; BESSER: Wir erwarten eine Produktivitätssteigerung um 10% zum Vorjahr)
			<p>&nbsp;<p>
			A - Attraktiv: Ziele sollen begeistern und motivieren. Welche positiven Effekte bringt das Projekt in unseren Arbeitsalltag?
			<p>&nbsp;<p>
			R: realistisch: Ziele sollen fordern, jedoch NICHT überfordern. Formuliere erreichbare Ziele.
			<p>&nbsp;<p>
			T - terminiert: Definiere für das Ziel einen konkreten Umsetzungszeitpunkt (SCHLECHT: Wir wollen nächstes Jahr am Ziel sein; BESSER: Das Go-Live erfolgt am 23.02.XX)  ',
			'prerequisites_dependencies_and_exclusions_tooltip' =>'',
			'alternatives_options_tooltip' =>'',
			'milestones_tooltip' =>'Meilensteine sind Entscheidungszeitpunkte im Projektverlauf und werden meist am Ende von Projektphasen definiert. Generell kann ein Meilenstein ein Ereignis sein, an dem: <br>&nbsp;<br>
- etwas abgeschlossen ist,<br>
- etwas begonnen wird oder<br>
- über die weitere Vorgehensweise entschieden wird.<p>&nbsp;<p>
Meilensteine verlaufen nie über eine Zeitdauer sondern erfordern punktuelle Entscheidungen.',
			'required_resources_tooltip' =>'',
			'project_members_tooltip' =>'Wer arbeitet an dem Projekt aktiv mit? Bitte verwende den vollständigen Namen (Vorname Nachname).',
			'add_public_documents_tooltip' =>'Die hier hochgeladenen Dokumente sind von allen Lyreco Mitarbeitern einsehbar.',
			'add_internal_documents_tooltip' =>'Die hier hochgeladenen Dokumente sind dem Senior-Management (SMT) vorbehalten. Andere Mitarbeiter sehen diese Dokumente nicht.',
		
		'setting'=>'die Einstellungen',
		'request_for_team_member'=>'Besucher können sich für die Mitarbeit am Projekt bewerben.',
		
		'links' => 'Links',
		'add_link_tooltip' => '',
		'add_link_heading' => 'LINK HINZUFÜGEN',
		'link_title' => 'Link-Titel:',
		'link_title_placeholder' => 'Link-Titel eingeben (max. 50 Zeichen)',
		'url' => 'URL:',
		'url_placeholder' => 'http://',
		'is_public' => 'Öffentlich?',
		'save_link' => 'HINZUFÜGEN',

		'i_approve_project_details' => 'Ich genehmige die Projektdetails von oben',
		'sponsor_signoff' => 'SPONSOR ABMELDUNG',
		'sponsor_signoff_pending' => 'steht aus',
		'draft' => 'Entwurf',

		'skilled_members' => 'Qualifizierte Mitglieder',
		'draft_status' => 'Entwurfsstatus',
		'signedoff_note' => 'Projektdetails genehmigt am :date von :user',

		'signoff_note' => 'HINWEIS: Als Sponsor müssen Sie die oben genannten Informationen bereitstellen und genehmigen, bevor der Projektmanager mit der Arbeit an dem Projekt beginnt. Einmal dein
		 Wenn ein Singoff erstellt wurde, ist das Projekt für andere Benutzer sichtbar. Dieser Schritt kann nicht rückgängig gemacht werden. Sie können die Projektdetails nachher noch ändern
		 Die Freigabe bei Bedarf, aber die Freigabe stellt die Grundlage dar, auf die sich alle Projektmitglieder und Stakeholder geeinigt haben',
		'project_manager_signed_off_note'=>'HINWEIS: Ihr Sponsor muss die oben genannten Informationen genehmigen, bevor Sie mit der Arbeit an dem Projekt beginnen. Der Sponsor muss die oben genannten Informationen bereitstellen, aber Sie können ihn mit Ihrem Fachwissen unterstützen. Sobald sein Singoff gemacht wurde, wird das Projekt für andere Benutzer sichtbar sein. Sie können
		 Ändern Sie die Projektdetails nach der Freigabe bei Bedarf, aber die Freigabe stellt die Grundlage dar, auf der alle Projektmitglieder und Stakeholder basieren
		 vereinbart.',
	],
	
	'project_status' => [
		'latest_status_updates' => 'Status Updates',
		'status_updates' => 'Status Update',
		'send_remind' => 'Erinnerung senden',
		'send_reminder_heading' => 'Erinnerung senden',
		'reminder_sent' => 'EMAILS WURDEN VERSCHICKT',
		'remind_all' => 'Alle erinnern',
		'yes' => 'Ja',
		'no' => 'Nein',
		'projects' => 'PROJEKTE',
		'department' => 'BUSINESS UNIT',
		'sponsor' => 'SPONSOR',
		'group' => 'GRUPPE',
		'latest_update' => 'LETZTES <br/> UPDATE',
		'action' => 'AKTION',
		'reminder_alert' => 'Soll an den Projektleiter des Projekts ":project" eine Erinnerung geschickt werden?',
		'reminder_alert_all_heading' => 'ALLE ERINNERN',
		'reminder_alert_all' => 'Soll eine Email an alle Projektleiter mit überfälligen Projektupdates geschickt werden?',
		'cost'=>'KOSTEN',		
		'quality'=>'QUALITÄT',	
		'time'=>'ZEIT',
	    'send_email'=>'EMAIL SCHICKEN',		
		
        'inform' => 'informieren',
		'inform_all' => 'alle informieren',
		'inform_alert' => 'Soll der Projektleiter des Projektes ":project" via Email informiert werden?',
		'inform_all_heading' => 'Projektleiter informieren',
		'inform_heading' => 'Projektleiter informieren',
		'inform_alert_all' => 'Es wird eine Email an alle Projektleiter verschickt, bei denen Projekte mit bereits erreichtem Enddatum immernoch aktiv sind. Sofern das Projekt beendet ist, muss der Projektleiter das PMO darüber informieren, damit das Projekt archiviert wird.',	
		'inform_email' => 'EMAIL SCHICKEN',			
		'informed' => 'EMAIL(S) VERSCHICKT',			
	],

    'my_project' => [
		'd' => 'D',
		'project_management' => 'Meine Projekte',
		'projects' => 'PROJEKTE',
		'department' => 'BUSINESS UNIT',
		'sponsor' => 'SPONSOR',
		'active' => 'AKTIV',
		'yes' => 'Ja',
		'no' => 'Nein',
		'group' => 'GRUPPE',
		'latest_update' => 'LETZES <br/> UPDATE',
		'action' => 'AKTION',
		'cost'=>'KOSTEN',		
		'quality'=>'QUALITÄT',		
		'time'=>'ZEIT',
		'modify' => 'Details ändern',
		'update_status' => 'Status ändern',		
	],

	'sponsored_project' => [
		'project_management' => 'Gesponserte Projekte',
		'projects' => 'PROJEKTE',
		'department' => 'BUSINESS UNIT',
		'sponsor' => 'SPONSOR',
		'active' => 'AKTIV',
		'yes' => 'Ja',
		'no' => 'Nein',
		'latest_update' => 'LETZES <br/> UPDATE',
		'action' => 'AKTION',
		'cost'=>'KOSTEN',
		'quality'=>'QUALITÄT',
		'time'=>'ZEIT',
		'modify_settings' => 'Einstellungen',
		'modify_details' => 'Details ändern',
		'pic' => 'Bild',
		'show_archived' => 'Archiv anzeigen',
		'project_manager' => 'PM',
		'group' => 'G',
		'active' => 'A',
		'public' => 'P',
		'note' => 'HINWEIS: Dein Projekt ist abgeschlossen oder wurde eingestellt? Sende eine E-Mail an <a href="mailto:lprt@lyreco.swiss"> lprt@lyreco.swiss </a> um das Projekt zu archivieren/löschen.'
	],

	'all_projects' => [
		'project_management' => 'Gesponserte Projekte',
		'project_title' => 'PROJEKTE',
		'department' => 'BUSINESS UNIT',
		'sponsor' => 'SPONSOR',
		'start' => 'START',
		'end' => 'ENDE',
		'active' => 'AKTIV',
		'yes' => 'Ja',
		'no' => 'Nein',
		'latest_update' => 'LETZES <br/> UPDATE',
		'action' => 'AKTION',
		'cost'=>'KOSTEN',
		'quality'=>'QUALITÄT',
		'time'=>'ZEIT',
		'modify_settings' => 'Einstellungen',
		'modify_details' => 'Details ändern',
		'pic' => 'Bild',
		'show_archived' => 'Archiv anzeigen',
		'pm' => 'PM',
		'group' => 'G',
		'active' => 'A',
		'public' => 'P',
		'd' => 'D',
		'p' => 'P',
		'sign_off' => 'Abmelden',
		'sign_off_pending' => 'steht aus',
	],

		'excel'=>[
				'project_name' => 'Projektbezeichnung',
				'business_unit' => 'Business Unit',
				'sponsor_name' => 'Sponsor',
				'sponsor_email' => 'Sponsor Email',
				'project_manager' => 'Projektleiter',
				'project_manager_email' => 'Projektmanager E-Mail',
				'estimated_start_date' => 'Startdatum (geplant)',
				'estimated_end_date' => 'Enddtatum (geplant)',
				'public' => 'Öffentlich?',
				'group' => 'Gruppenprojekt?',
				'active' => 'Aktiv?',
				'archive' => 'Archiviert?',
				'picture' => 'Bild URL',
				'project_description' => 'Projektbeschreibung',
				'current_situation' => 'Ausgangslage',
				'project_objective' => 'Projektziel(e)',
				'prerequisite_dependencies_exclusions' => 'Voraussetzungen/Abhängigkeiten/Ausschlüsse',
				'alternative_or_options' => 'Alternativen/Optionen',
				'milestones' => 'Meilensteine',
				'required_resources' => 'Erforderliche Ressourcen',
				'overall_status' => 'Gesamtstatus',
				'percentage_completion' => 'Fertigstellungsgrad',
				'real_start_date' => 'Start (tatsächlich)',
				'real_end_date' => 'Ende (tatsächlich)',
				'current_quality' => 'Status Qualität',
				'current_quality_explanation' => 'Status Qualität Details',
				'cost_situation' => 'Status Kosten',
				'cost_situation_explanation' => 'Status Kosten Details',
				'time_planning' => 'Status Zeit',
				'time_planning_explanation' => 'Status Zeit Details',
				'documents' => 'Angefügte Dokumente',
				'public_document' => 'Dokumente (öffentlich)',
				'private_document' => 'Dokumente (intern)',
				'project_members' => 'Projektmitglieder',
				'find_skilled_team_members' => 'Finden Sie qualifizierte Teammitglieder',
				'good' => 'Gut',
				'average' => 'Mittel',
				'bad' => 'Schlecht',
				'yes' => 'Ja',
				'no' => 'Nein',
		],
		'settings' =>[
				'general_settings_title' => 'Allgemeine Einstellungen',
				'standard_language' => 'Standardsprache',
				'link_settings_title' => 'Linkeinstellungen',
				'guest_autologin_link' => 'Gast AutoLogin Link',
				'guest_autologin_link_placeholder' => '',
				'talent_link' => 'Talent Link',
				'talent_link_placeholder' => '',
				'resources_link' => 'Ressourcen Link',
				'resources_link_placeholder' => '',
				'user_manual_link' => 'Benutzerhandbuch Link',
				'user_manual_link_placeholder' => '',
				'email_settings_title' => 'Email Einstellungen',
				'auto_reminder_email_activated' => 'Automatische Erinnerungs-E-Mail aktiviert',
				'reminder_email_threshold' => 'Erinnerungs-E-Mail-Schwellenwert',
				'reminder_email_threshold_placeholder' => '',
				'project_settings_title' => 'Projekt Einstellungen',
				'new_icon_threshold' => 'Neuer Symbolschwellenwert',
				'new_icon_threshold_placeholder' => '',
				'sponsor_can_create_projects' => 'Der Sponsor kann Projekte erstellen',
				'settings_saved' => 'Einstellungen erfolgreich gespeichert',
				'details_not_saved' => 'Nicht gespeichert. Bitte überprüfen Sie fehlende / ungültige Informationen',
				'save'=>'SAVE'
		],

	// Reporting Panel
	'reporting_panel' => 'Reports',
	'select_file' => 'Datei wählen',
	'uploading' => 'Bild wird übertragen … bitte warten',
	'image_mime' => 'Das Bild muss im .jpg oder .png Format sein.',
	'image_size' => 'Bildgrösse max. 20 MB',
	'delete_image' => 'Bild löschen',
	'save_image_changes' => 'SPEICHERN',


	// User Panel
	'user_panel' => 'BENUTZER',
	'my_profile' => 'Mein Profil',
	'name' => 'Name',
	'role' => 'Rolle',
	'job_title' => 'Job Titel:',
	'job_title_placeholder' => 'Bitte Job Titel eintragen',
	'select_image' => 'Profilbild wählen',
	'password_management' => 'Passwort Management',
	'current_password' => 'Aktuelles Passwort:',
	'new_password' => 'Neues Passwort:',
	're_enter_new_password' => 'Passwort wiederholen:',
	'save_changes' => 'SPEICHERN',
   'profile_image_upload_modal_title'=>'Bild wählen',
	
    'good' => 'Gut',
	'average' => 'Mittel',
	'bad' => 'Schlecht',
];
// de
?>
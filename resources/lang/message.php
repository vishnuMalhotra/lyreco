<?php

return [
    'server_error' => 'Something went wrong, please try again later.',
    'project_deleted' => 'Project deleted.',

];
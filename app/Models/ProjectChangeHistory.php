<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectChangeHistory extends Model
{
    protected $table = "project_change_history";
    protected $primaryKey = "id";
    protected $fillable = ["fk_projectId", "fk_userId", "change_reason", "project_description", "current_situation", "project_objective",
        "prerequisite_dependencies_exclusions", "alternative_or_options", "milestones", "required_resources", "project_members"];

    public $sortOrder = 'asc';
    public $sortEntity = 'project_change_history.id';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\Request;
use App\Models\ProjectMember;
use App\Models\ProjectDocument;
use Validator;
use Carbon\Carbon;
use App\Models\Role;
use App\User;
use Mail;
use App\Models\ProjectStatus;
use App\Models\ProjectChangeHistory;
use App\Models\ProjectLink;


class Project extends Model
{
    protected $table = "projects";
    protected $primaryKey = "id";
    protected $fillable = ['project_title', "fk_businessUnitId", "project_manager", "estimated_start_date",
        "estimated_end_date", "is_public", "is_group", "is_active", "is_archive", "picture", "project_description", "current_situation", "project_objective","sign_off_by",
        "prerequisite_dependencies_exclusions", "alternative_or_options", "fk_sponsorId","milestones", "required_resources", "sponsor_signed_off", "sign_off_date", "request_for_team_member"];

    public $sortOrder = 'asc';
    public $sortEntity = 'projects.id';

    public function projectBasicDetailsValidation($request)
    {
        $inputs = $request->all();
		if(isset($inputs['estimated_start_date'])){ 
			$inputs['estimated_start_date'] = Carbon::createFromFormat('d/m/y', $inputs['estimated_start_date'])->format('Y-m-d');
		}
		if(isset($inputs['estimated_end_date'])){
				$inputs['estimated_end_date'] = Carbon::createFromFormat('d/m/y', $inputs['estimated_end_date'])->format('Y-m-d');
		} 
        $rules = [
            'project_title' => 'required',
            'fk_sponsorId' => 'required',
          //  'sponsor_email' => 'required',
            'project_manager' => 'required',
            'estimated_start_date' => 'required|before:estimated_end_date',
            'estimated_end_date' => 'required|after:estimated_start_date',
			'fk_businessUnitId' => 'required',
        ];
        if(!isset($inputs['id']) and $inputs['id'] == null) {
            //$rules = $rules + ['fk_businessUnitId' => 'required'];
        }

        return $validator = Validator::make($inputs, $rules);
    }

    public function saveProjectBasicDetails($request)
    {
        $inputs = $request->input();
		if(isset($inputs['estimated_start_date'])){ 
			$inputs['estimated_start_date'] = Carbon::createFromFormat('d/m/y', $inputs['estimated_start_date'])->format('Y-m-d');
		}
		if(isset($inputs['estimated_end_date'])){
				$inputs['estimated_end_date'] = Carbon::createFromFormat('d/m/y', $inputs['estimated_end_date'])->format('Y-m-d');
		} 
		$projectDetails = $this->find($inputs['id']);
        if($request->hasFile('picture')){
            $image = $request->hasFile('picture');
            $folder = 'products/';
            $inputs['picture'] = uploadImage($request->file('picture'),$folder);
        }

        if(isset($inputs['is_public'])) {
            $inputs['is_public'] = 1;
        } else{
            $inputs['is_public'] = 0;
        }

        if(isset($inputs['is_group'])) {
            $inputs['is_group'] = 1;
        } else{
            $inputs['is_group'] = 0;
        }

        if(isset($inputs['is_active'])) {
            $inputs['is_active'] = 1;
        } else{
            $inputs['is_active'] = 0;
        }

        $emailDetails = $sponsorDetails = [];
		if( $inputs['id'] == null) {
            $rolesIds = Role::where('alerts', 1)->pluck('id')->toArray();
            $emailDetails = User::where('is_active', "1")->select('name', 'email', 'is_active')
                ->where(function($query1) use ($rolesIds, $inputs){
                    $query1->whereIn('role', $rolesIds)->orWhere('id', $inputs['project_manager']);
                })->get()->toArray();
            $sponsorDetails = User::where('id', $inputs['fk_sponsorId'])->where('is_active', "1")->select('name as sponsor_name', 'email as sponsor_email')->first();
        } else{
            if(isset($inputs['fk_sponsorId']) and $inputs['fk_sponsorId'] != null){
                $sponsorDetails = User::where('id', $inputs['fk_sponsorId'])->where('is_active', "1")->select('name as sponsor_name', 'email as sponsor_email')->first();
                if($projectDetails['fk_sponsorId'] != null) {
                    if($inputs['fk_sponsorId'] != $projectDetails['fk_sponsorId']) {
                        $emailDetails[] = ['name'=>$sponsorDetails['sponsor_name'], 'email'=>$sponsorDetails['sponsor_email'], 'type'=>'sponsor'];
                    }
                } else{
                    $emailDetails[] = ['name'=>$sponsorDetails['sponsor_name'], 'email'=>$sponsorDetails['sponsor_email'], 'type'=>'sponsor'];
                }
            }
        }
		
        if(!isset($inputs['id']) and $inputs['id'] == null) {
            if(isset($inputs['fk_sponsorId']) ){
                $emailDetails[] = ['name'=>$sponsorDetails['sponsor_name'], 'email'=>$sponsorDetails['sponsor_email'], 'type'=>'sponsor'];
            } 
			$projectManagerDetail =  User::where('id', $inputs['project_manager'])->select('name', 'email')->first();
            $emailDetails[] = ['name'=>$projectManagerDetail['name'], 'email'=>$projectManagerDetail['email']];
        }
        $project =  Project::updateOrCreate(['id'=>$inputs['id']], $inputs);
        if($inputs['id'] == null){
           $projectStatus['real_start_date'] = $inputs['estimated_start_date'];
           $projectStatus['realistic_end_date'] = $inputs['estimated_end_date'];
           ProjectStatus::updateOrCreate(['fk_projectId'=>$project['id']], $projectStatus);
        }

        if(count($emailDetails) != 0) {
            foreach($emailDetails as $emailDetail) {
				if(isset($emailDetail['type']) and $emailDetail['type'] == 'sponsor' and $inputs['id'] != null) {
					$content = EmailTemplate::find(7);
				} else{
					$content = EmailTemplate::find(2);
				}
                $date = Carbon::now()->timezone(\Cookie::get('timezone'))->format('d-m-y');
                $time = Carbon::now()->timezone(\Cookie::get('timezone'))->format('h:i:s');
                $address = $emailDetail['email'];
                $name = $emailDetail['name'];
                $subject = str_replace('[PROJECT TITLE]', $project->project_title, $content->template_subject);;

                $email_content = str_replace('[RECEIVER NAME]', $name, $content->template_content);

                $email_content = str_replace('[USER NAME]', \Auth::user()->name, $email_content);
                $email_content = str_replace('[DD-MM-YY]', $date, $email_content);
                $email_content = str_replace('[HH:MM:SS]', $time, $email_content);

                $email_content = str_replace('[PROJECT TITLE]', $project->project_title, $email_content);
                $email_content = str_replace('[PROJECT LINK]', '<a href="'.url('about/project/'.$project->id).'">'.url('about/project/'.$project->id)."</a>", $email_content);

                $data['test_message'] = $email_content;
                Mail::send('emails.test', $data, function($message) use($address, $name, $subject) {
                    $message->to($address, $name)->subject($subject);
                });
            }
        }
        return $project;
    }

    public function projectDetailsValidation($request)
    {
        $rules = [
            'project_description' => 'required',
            'current_situation' => 'required',
            'project_objective' => 'required',
            'prerequisite_dependencies_exclusions' => 'required',
            'alternative_or_options' => 'required',
            'milestones' => 'required',
            'required_resources' => 'required'
        ];
        return validator($request, $rules);
    }

    public function updateProjectDetail($request)
    {
        $inputs = $request;
        $projectDetails = $this->leftjoin('users', 'users.id', '=', 'projects.fk_sponsorId')->where('projects.id', $inputs['id'])
        ->select('projects.*', 'users.name as sponsor_name', 'users.email as sponsor_email')->first();
        if($projectDetails == null){
            return "404";
        }
        $projectDetails['change_reason'] = $inputs['change_reason'];
        $this->sendProjectMail($projectDetails);

        unset($inputs['members']);
        unset($inputs['_token']);
        if(isset($inputs['request_for_team_member'])) {
            $inputs['request_for_team_member'] = 1;
        } else {
            $inputs['request_for_team_member'] = 0;
        }

        if(isset($inputs['sponsor_signed_off'])) {
            $inputs['sponsor_signed_off'] = 1;
            $inputs['sign_off_by'] = \Auth::user()->id;
            $this->projectSignOffEmail($projectDetails);
        }
        $project = Project::updateOrCreate(['id'=>$inputs['id']], $inputs);

		if(isset($inputs['linkId']) and count($inputs['linkId']) > 0) {
			ProjectLink::whereNotIn('id', $inputs['linkId'])->delete();
			for($i=0; $i<count($inputs['linkId']); $i++) {
				ProjectLink::updateOrCreate(['id'=>$inputs['linkId'][$i]], 
			['title'=>$inputs['title'][$i], 
				'url'=>$inputs['url'][$i], 
				'is_public'=>$inputs['url_is_public'][$i], 
				'fk_projectId'=>$project->id]);
			}
		} else {
			ProjectLink::where('fk_projectId', $project->id)->delete();
		}
        $inputs['fk_projectId'] = $project->id;
        $inputs['fk_userId'] = \Auth::user()->id;
        $project_members = ProjectMember::where('fk_projectId', $project->id)->orderBy('fk_username', 'asc')->pluck('fk_username')->toArray();
        $inputs['project_members'] = implode(',', $project_members);
        ProjectChangeHistory::create($inputs);

        return "200";
    }

    public function validateProjectDetails($request)
    {
        $rules = [
            'project_title' => 'required',
            'project_manager' => 'required',
            'estimated_start_date' => 'required',
            'estimated_end_date' => 'required',
            'project_description' => 'required',
            'current_situation' => 'required',
            'project_objective' => 'required',
            'prerequisite_dependencies_exclusions' => 'required',
            'alternative_or_options' => 'required',
            'required_resources' => 'required',
            'milestones' => 'required',
        ];
        return $validator = Validator::make($request->all(), $rules);
    }

    public function sendProjectMail($project=null){
        if(!empty($project)) {
            $emailDetails = [];
           // $rolesIds= Role::where('alerts', 1)->pluck('id')->toArray();
            if($project['sponsor_email'] != null) {
                $emailDetails[] = ['name'=>$project['sponsor_name'], 'email'=>$project['sponsor_email']];
            }
			
			$projectManagerDetail =  User::where('id', $project['project_manager'])->select('name', 'email')->first();
			if($projectManagerDetail != null) {
				$emailDetails[] = ['name'=>$projectManagerDetail['name'], 'email'=>$projectManagerDetail['email']];
			}

            if($project['change_reason'] == 'sentence.initial_filling') {
				$project['change_reason'] = __($project['change_reason']);  
			}
			
            if(count($emailDetails) > 0) {
                $content = EmailTemplate::find(6);
                foreach($emailDetails as $emailDetail) {
                    $address = $emailDetail['email'];
                    $name = $emailDetail['name'];
                    $date = Carbon::now()->timezone(\Cookie::get('timezone'))->format('d-m-y');
                    $time = Carbon::now()->timezone(\Cookie::get('timezone'))->format('h:i:s');

                    $subject       = str_replace('[PROJECT TITLE]', $project->project_title, $content->template_subject);
                    $email_content = str_replace('[RECEIVER NAME]', $name, $content->template_content);
                    $email_content = str_replace('[PROJECT TITLE]', $project->project_title, $email_content);
                    $email_content = str_replace('[USER NAME]', \Auth::user()->name, $email_content);
                    $email_content = str_replace('[DD-MM-YY]', $date, $email_content);
                    $email_content = str_replace('[HH:MM:SS]', $time, $email_content);
                    $email_content = str_replace('[CHANGE REASON]', $project['change_reason'], $email_content);
                    $email_content = str_replace('[PROJECT LINK]', '<a href="'.url('about/project/'.$project->id).'">'.url('about/project/'.$project->id)."</a>", $email_content);

					$data['test_message'] = $email_content;
 
                    Mail::send('emails.test', $data, function($message) use($address, $name, $subject) {
                        $message->to($address, $name)->subject($subject);
                    });
                }   
            }
        }
    }

    public function sendProjectHealthMail($project=null){
        if(!empty($project)) {

            $rolesIds = Role::where('alerts', 1)->pluck('id')->toArray();
            $emailDetails = User::whereIn('role', $rolesIds)->select('name', 'email')->get()->toArray(); 
            if($project['sponsor_email'] != null) {
                $emailDetails[] = ['name'=>$project['sponsor_name'], 'email'=>$project['sponsor_email']];
            }


            if(count($emailDetails) > 0){
                $content = EmailTemplate::find(5);
                foreach($emailDetails as $emailDetail) {
                    $address = $emailDetail['email'];
                    $name = $emailDetail['name'];

                    $projectManagerName = User::where('id',$project->project_manager)->pluck('name')->first();
                    $subject       = str_replace('[PROJECT TITLE]', $project->project_title, $content->template_subject); 
                    $email_content = str_replace('[RECEIVER NAME]', $name, $content->template_content);
                    $email_content = str_replace('[PROJECT TITLE]', $project->project_title, $email_content);
                    $email_content = str_replace('[PROJECT MANAGER NAME]', $projectManagerName, $email_content);
                    $email_content = str_replace('[PROJECT LINK]', "<a href ='".url('about/project/'.$project->id)."'> ".url('about/project/'.$project->id)." </a>", $email_content);
                    $data['test_message'] = $email_content;

                    Mail::send('emails.test', $data, function($message) use($address, $name, $subject) {
                        $message->to($address, $name)->subject($subject);
                    });

                }
            }

        }
        return "success";
    }

    public function projectSignOffEmail($project)
    {
            $emailDetails = [];
            if($project['sponsor_email'] != null) {
                $emailDetails[] = ['name'=>$project['sponsor_name'], 'email'=>$project['sponsor_email']];
            }

            $projectManagerDetail =  User::where('id', $project['project_manager'])->select('name', 'email')->first();
            if($projectManagerDetail != null) {
                $emailDetails[] = ['name'=>$projectManagerDetail['name'], 'email'=>$projectManagerDetail['email']];
            }

            if($project['change_reason'] == 'sentence.initial_filling') {
                $project['change_reason'] = __($project['change_reason']);
            }

            if(count($emailDetails) > 0) {
                $content = EmailTemplate::find(11);
                foreach($emailDetails as $emailDetail) {
                    $address = $emailDetail['email'];
                    $name = $emailDetail['name'];
                    $subject       = str_replace('[PROJECT TITLE]', $project->project_title, $content->template_subject);
                    $email_content = str_replace('[RECEIVER NAME]', $name, $content->template_content);
                    $email_content = str_replace('[PROJECT TITLE]', $project->project_title, $email_content);
                    $email_content = str_replace('[SPONSOR NAME]', \Auth::user()->name, $email_content);
                    $data['test_message'] = $email_content;
                    Mail::send('emails.test', $data, function($message) use($address, $name, $subject) {
                        $message->to($address, $name)->subject($subject);
                    });
                }
            }
    }
}

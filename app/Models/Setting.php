<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = "settings";
    protected $primaryKey = "id";
    protected $fillable = ['fallback_language', 'guest_login_link', 'talented_team_members', 'pm_resource_base_url', 'user_manual_link',
        'activate_email_autoreminder', 'reminder_email_threshold_days', 'new_icon_threshold', 'sponsor_can_create_projects'];
}

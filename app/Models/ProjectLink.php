<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLink extends Model
{
    protected $table = "project_links";
    protected $primaryKey = "id";
    protected $fillable = ['fk_projectId', "title", "url", "is_public"];


}

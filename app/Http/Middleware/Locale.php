<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\App;
use Closure;
use Auth;

class Locale
{

    public function handle($request, Closure $next){
       \App::setlocale($request->cookie('locale'));
       \App::setFallbacklocale(settings('fallback_language'));
        return $next($request);
    }


}

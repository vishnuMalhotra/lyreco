<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
    public function viewSettings()
    {
        $settings = Setting::find(1);
        return view('backend.settings.view', compact('settings'));
    }

    public function saveSettings(Request $request)
    {
        try
        {
            \DB::beginTransaction();
                $input = $request->input();
                unset($input['_token']);
                if(isset($input['sponsor_can_create_projects'])) {
                    $input['sponsor_can_create_projects'] = 1;
                } else{
                    $input['sponsor_can_create_projects'] = 0;
                }

                if(isset($input['activate_email_autoreminder'])) {
                    $input['activate_email_autoreminder'] = 1;
                } else{
                    $input['activate_email_autoreminder'] = 0;
                }

                Setting::where('id', 1)->update($input);
                $extra['redirect'] = route('viewSettings');
            \DB::commit();
            return webResponse(true, 200, __('sentence.settings.settings_saved'), $extra);
        } catch (\Exception $e)
        {
            \DB::rollBack();
            return webResponse(false, 207, __('message.server_error'.$e));
        }
    }
}
